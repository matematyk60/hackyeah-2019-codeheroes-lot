[%bs.raw {|require("intl")|}];
[%bs.raw {|require("intl/locale-data/jsonp/en")|}];

[@bs.module]
external enLocaleData: ReactIntl.localeData('t) = "react-intl/locale-data/en";

ReactIntl.addLocaleData(enLocaleData);

type t =
  | En;

let toCountryCode =
  fun
  | En => "en";
