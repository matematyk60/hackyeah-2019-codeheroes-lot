[];
module MyBookings = [%graphql
  {|
  query MyBookings($userId: String!) {
    myBookings(userId: $userId) {
      id
      offer {
        id
        from
        to_: to
        outboundFlight {
          departureAirport
          arrivalAirport
          flightTime
          price
          departureTime
          arrivalTime
        }
        inboundFlight {
          departureAirport
          arrivalAirport
          flightTime
          price
          departureTime
          arrivalTime
        }
        totalPrice
        outboundDate
        returnDate
      }
    }
  }
|}
];

module QueryComponent = ReasonApollo.CreateQuery(MyBookings);

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~userId, children) => {
  ...component,
  render: _ => {
    let variables = MyBookings.make(~userId, ())##variables;

    let%Epitath {result, _} =
      children =>
        <QueryComponent variables fetchPolicy="cache-and-network">
          ...children
        </QueryComponent>;

    children(result);
  },
};
