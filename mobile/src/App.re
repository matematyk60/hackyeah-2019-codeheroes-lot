module TextComponent = {
  let component = ReasonReact.statelessComponent(__MODULE__);
  let make = (children: string) => {
    ...component,
    render: _ => ReasonReact.string(children),
  };

  [@bs.deriving abstract]
  type props = {children: string};

  let textComponent =
    ReactIntl.ReactComponent(
      ReasonReact.wrapReasonForJs(~component, props =>
        make(props->childrenGet)
      ),
    );
};

let styles =
  BsReactNative.(
    StyleSheet.create(
      Style.{
        "container":
          style([
            flex(1.0),
            width(Pct(100.0)),
            paddingTop(
              switch (Platform.os()) {
              | Platform.IOS(_) => Pt(20.0)
              | _ => Pt(0.0)
              },
            ),
          ]),
      },
    )
  );

let component = ReasonReact.statelessComponent(__MODULE__);
let make = _ => {
  ...component,
  render: _ => {
    let%Epitath context = children =>
      <SessionContext> ...children </SessionContext>;

    <>
      <BsReactNative.StatusBar barStyle=`darkContent />
      <ReasonApollo.Provider client=GraphQLClient.client>
        <ReactIntl.IntlProvider
          textComponent=TextComponent.textComponent
          locale={Locale.toCountryCode(En)}>
          <BsReactNative.SafeAreaView style=styles##container>
            <NotificationsManager context />
            <Navigation context />
          </BsReactNative.SafeAreaView>
        </ReactIntl.IntlProvider>
      </ReasonApollo.Provider>
    </>;
  },
};

let _ = [%bs.raw {|console.disableYellowBox = true|}];

let app = ReasonReact.wrapReasonForJs(~component, _ => make([||]));
