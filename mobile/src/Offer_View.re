module Mutation = {
  module Create = Booking_Mutation_Create;
};

let messages =
  [@intl.messages]
  {
    "finishReservation": {
      "id": "offer.finishReservation",
      "defaultMessage": {j|Dokończ rezerwacje|j},
    },
    "book": {
      "id": "offer.book",
      "defaultMessage": {j|ZARAZERWUJ|j},
    },
    "date": {
      "id": "offer.date",
      "defaultMessage": {j|DATA|j},
    },
    "flightNumber": {
      "id": "offer.flightNumber",
      "defaultMessage": {j|NUMER LOTU|j},
    },
    "passengers": {
      "id": "offer.passengers",
      "defaultMessage": {j|Liczba pasażerów|j},
    },
    "total": {
      "id": "offer.total",
      "defaultMessage": {j|RAZEM|j},
    },
  };

let logo = `Required(BsReactNative.Packager.require("./assets/lot.png"));
let plane = `Required(BsReactNative.Packager.require("./assets/plane.png"));

let styles =
  BsReactNative.(
    StyleSheet.create(
      Style.{
        "header":
          style([
            padding(Pt(16.0)),
            backgroundColor(Theme.primary),
            height(Pt(100.0)),
          ]),
        "headerText":
          style([
            marginTop(Pt(16.0)),
            fontWeight(`_600),
            color(Theme.light),
            fontSize(Float(15.0)),
          ]),
        "container": style([flex(1.0), backgroundColor(Theme.primary)]),
        "content":
          style([
            backgroundColor(Theme.light),
            flex(1.0),
            borderTopLeftRadius(20.0),
            borderTopRightRadius(20.0),
          ]),
        "innerContent": style([flex(1.0)]),
        "buttonWrapper":
          style([
            width(Pct(100.0)),
            justifyContent(Center),
            marginTop(Pt(64.0)),
            flexDirection(Row),
          ]),
        "buttonText": style([color(Theme.light)]),
        "button":
          style([
            height(Pt(10.0)),
            width(Pt(100.0)),
            backgroundColor(Theme.error),
          ]),
        "card":
          style([
            margin(Pt(16.0)),
            borderRadius(8.0),
            padding(Pt(16.0)),
            backgroundColor(Theme.light),
            shadowColor(String("rgba(100, 100, 100, 0.25)")),
            shadowRadius(15.0),
            shadowOffset(~height=3.0, ~width=-3.0),
            shadowOpacity(0.5),
          ]),
        "cardTop":
          style([
            flexDirection(Row),
            justifyContent(SpaceBetween),
            alignItems(Center),
          ]),
        "timeAirport": style([justifyContent(Center), alignItems(Center)]),
        "flightTime": style([justifyContent(Center), alignItems(Center)]),
        "planeIcon": style([marginBottom(Pt(4.0))]),
        "time": style([fontSize(Float(23.0)), fontWeight(`_500)]),
        "airport": style([fontSize(Float(19.0)), fontWeight(`_300)]),
        "label":
          style([
            marginBottom(Pt(4.0)),
            fontSize(Float(9.0)),
            color(Theme.borderGrey),
          ]),
        "value": style([fontSize(Float(15.0))]),
        "cardBottom":
          style([
            flexDirection(Row),
            justifyContent(SpaceBetween),
            marginTop(Pt(32.0)),
          ]),
        "passengers":
          style([
            marginBottom(Pt(8.0)),
            fontWeight(`_500),
            fontSize(Float(14.0)),
            color(Theme.primary),
          ]),
        "passengersValue":
          style([
            fontSize(Float(12.0)),
            marginRight(Pt(16.0)),
            marginTop(Pt(8.0)),
          ]),
        "passengersWrapper":
          style([
            borderColor(Theme.borderGrey),
            borderBottomWidth(1.0),
            margin(Pt(16.0)),
            marginTop(Pt(0.0)),
            marginBottom(Pt(0.0)),
            paddingBottom(Pt(16.0)),
          ]),
        "passengersValueWrapper": style([flexDirection(Row)]),
        "totalWrapper": style([flexDirection(Row), padding(Pt(16.0))]),
        "totalText":
          style([
            color(Theme.primary),
            fontWeight(`_600),
            marginRight(Pt(16.0)),
          ]),
        "loader": style([marginTop(Pt(32.0))]),
      },
    )
  );

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~navigate, ~chatbotId, ~userId, ~offerId, _) => {
  ...component,
  render: _ => {
    let%Epitath result = children =>
      <Offer_Query_Details offerId> ...children </Offer_Query_Details>;

    let%Epitath book = children =>
      <Booking_Mutation_Create> ...children </Booking_Mutation_Create>;

    <BsReactNative.View style=styles##container>
      <BsReactNative.View style=styles##header>
        <BsReactNative.Image source=logo />
        <Text message=messages##finishReservation style=styles##headerText />
      </BsReactNative.View>
      <BsReactNative.View style=styles##content>
        {switch (result) {
         | Loading => <BsReactNative.ActivityIndicator style=styles##loader />
         | Error(_) => ReasonReact.null
         | Data(data) =>
           switch (data##offer) {
           | None => ReasonReact.null
           | Some(offer) =>
             <BsReactNative.View style=styles##innerContent>
               <BsReactNative.View style=styles##card>
                 <BsReactNative.View style=styles##cardTop>
                   <BsReactNative.View style=styles##timeAirport>
                     <Text.Raw
                       content={
                         offer##outboundFlight##departureTime
                         ->Json.Decode.float
                         ->Time.toHourTime
                       }
                       style=styles##time
                     />
                     <Text.Raw
                       content={offer##outboundFlight##departureAirport}
                       style=styles##airport
                     />
                   </BsReactNative.View>
                   <BsReactNative.View style=styles##flightTime>
                     <BsReactNative.Image
                       source=plane
                       style=styles##planeIcon
                     />
                     <Text.Raw
                       content={
                         (
                           offer##outboundFlight##flightTime->Json.Decode.float
                           *. 60.
                           *. 1000.
                         )
                         ->Time.toDuration
                       }
                     />
                   </BsReactNative.View>
                   <BsReactNative.View style=styles##timeAirport>
                     <Text.Raw
                       content={
                         offer##outboundFlight##arrivalTime
                         ->Json.Decode.float
                         ->Time.toHourTime
                       }
                       style=styles##time
                     />
                     <Text.Raw
                       content={offer##outboundFlight##arrivalAirport}
                       style=styles##airport
                     />
                   </BsReactNative.View>
                 </BsReactNative.View>
                 <BsReactNative.View style=styles##cardBottom>
                   <BsReactNative.View>
                     <Text message=messages##date style=styles##label />
                     <Text.Raw
                       content={
                         offer##outboundFlight##departureTime
                         ->Json.Decode.float
                         ->Time.toDailyTime
                       }
                     />
                   </BsReactNative.View>
                   <BsReactNative.View>
                     <Text
                       message=messages##flightNumber
                       style=styles##label
                     />
                     <Text.Raw content="LOT123" />
                   </BsReactNative.View>
                 </BsReactNative.View>
               </BsReactNative.View>
               <BsReactNative.View style=styles##passengersWrapper>
                 <Text message=messages##passengers style=styles##passengers />
                 <BsReactNative.View style=styles##passengersValueWrapper>
                   <Text.Raw
                     content={j|2x Dorosły|j}
                     style=styles##passengersValue
                   />
                   {
                     let price =
                       offer##outboundFlight##price
                       ->Json.Decode.float
                       ->int_of_float
                       ->Money.formatPrice;
                     <Text.Raw
                       content={j|2x $price|j}
                       style=styles##passengersValue
                     />;
                   }
                 </BsReactNative.View>
               </BsReactNative.View>
               <BsReactNative.View style=styles##totalWrapper>
                 <Text message=messages##total style=styles##totalText />
                 <Text.Raw
                   content={
                     (offer##outboundFlight##price->Json.Decode.float *. 2.0)
                     ->int_of_float
                     ->Money.formatPrice
                   }
                   style=styles##totalText
                 />
               </BsReactNative.View>
               <BsReactNative.View style=styles##buttonWrapper>
                 <Button
                   caption=messages##book
                   onPress={() =>
                     book(~userId, ~offerId, ~chatbotId, ())
                     |> Repromise.wait(result =>
                          switch (result) {
                          | Result.Ok(_) =>
                            BsReactNative.Alert.alert(
                              ~title={j|System płatności|j},
                              ~buttons=[
                                {
                                  style: None,
                                  text: Some({j|Przejdź dalej|j}),
                                  onPress:
                                    Some(
                                      () => navigate(Route.Bookings(userId)),
                                    ),
                                },
                              ],
                              (),
                            )
                          | Result.Error(_) => ()
                          }
                        )
                   }
                 />
               </BsReactNative.View>
             </BsReactNative.View>
           }
         }}
      </BsReactNative.View>
    </BsReactNative.View>;
  },
};
