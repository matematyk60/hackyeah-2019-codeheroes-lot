let styles =
  BsReactNative.(
    StyleSheet.create(
      Style.{
        "header":
          style([
            padding(Pt(16.0)),
            backgroundColor(Theme.primary),
            height(Pt(100.0)),
          ]),
        "headerText":
          style([
            marginTop(Pt(16.0)),
            fontWeight(`_600),
            color(Theme.light),
            fontSize(Float(15.0)),
          ]),
        "container": style([flex(1.0), backgroundColor(Theme.primary)]),
        "content":
          style([
            backgroundColor(Theme.light),
            flex(1.0),
            borderTopLeftRadius(20.0),
            borderTopRightRadius(20.0),
          ]),
        "innerContent": style([flex(1.0)]),
        "card":
          style([
            margin(Pt(16.0)),
            borderRadius(8.0),
            padding(Pt(16.0)),
            backgroundColor(Theme.light),
            shadowColor(String("rgba(100, 100, 100, 0.25)")),
            shadowRadius(15.0),
            shadowOffset(~height=3.0, ~width=-3.0),
            shadowOpacity(0.5),
          ]),
        "cardTop": style([]),
        "checkin":
          style([
            paddingTop(Pt(16.0)),
            borderColor(Theme.borderGrey),
            borderTopWidth(1.0),
            flexDirection(Row),
            width(Pct(100.0)),
            justifyContent(FlexEnd),
          ]),
        "airport":
          style([
            marginTop(Pt(16.0)),
            marginBottom(Pt(16.0)),
            flexDirection(Row),
            justifyContent(SpaceBetween),
            width(Pct(70.0)),
          ]),
        "checkInText": style([color(Theme.mint)]),
        "loader": style([marginTop(Pt(32.0))]),
      },
    )
  );

let logo = `Required(BsReactNative.Packager.require("./assets/lot.png"));
let plane = `Required(BsReactNative.Packager.require("./assets/plane.png"));

let messages =
  [@intl.messages]
  {
    "title": {
      "id": "booking.finishReservation",
      "defaultMessage": {j|Moje rezerwacje|j},
    },
    "checkin": {
      "id": "booking.checkin",
      "defaultMessage": {j|CHECK IN|j},
    },
  };

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~userId, _) => {
  ...component,
  render: _ => {
    let%Epitath result = children =>
      <Booking_Query_All userId> ...children </Booking_Query_All>;

    <BsReactNative.View style=styles##container>
      <BsReactNative.View style=styles##header>
        <BsReactNative.Image source=logo />
        <Text message=messages##title style=styles##headerText />
      </BsReactNative.View>
      <BsReactNative.View style=styles##content>
        {switch (result) {
         | Loading => <BsReactNative.ActivityIndicator style=styles##loader />
         | Error(_) => ReasonReact.null
         | Data(data) =>
           Js.log(data##myBookings);
           <BsReactNative.ScrollView style=styles##innerContent>
             {data##myBookings
              ->Array.map(booking =>
                  <>
                    <BsReactNative.View style=styles##card>
                      <BsReactNative.View style=styles##cardTop>
                        <Text.Raw
                          content={Time.toFullTime(
                            booking##offer##outboundFlight##flightTime
                            ->Json.Decode.float,
                          )}
                        />
                        <BsReactNative.View style=styles##airport>
                          <Text.Raw
                            content={
                              booking##offer##from->Js.String.toUpperCase
                            }
                          />
                          <BsReactNative.Image source=plane />
                          <Text.Raw
                            content={
                              booking##offer##to_->Js.String.toUpperCase
                            }
                          />
                        </BsReactNative.View>
                      </BsReactNative.View>
                      <BsReactNative.TouchableOpacity style=styles##checkin>
                        <Text
                          message=messages##checkin
                          style=styles##checkInText
                        />
                      </BsReactNative.TouchableOpacity>
                    </BsReactNative.View>
                    <BsReactNative.View style=styles##card>
                      <BsReactNative.View style=styles##cardTop>
                        <Text.Raw
                          content={Time.toFullTime(
                            booking##offer##inboundFlight##flightTime
                            ->Json.Decode.float,
                          )}
                        />
                        <BsReactNative.View style=styles##airport>
                          <Text.Raw
                            content={
                              booking##offer##to_->Js.String.toUpperCase
                            }
                          />
                          <BsReactNative.Image source=plane />
                          <Text.Raw
                            content={
                              booking##offer##from->Js.String.toUpperCase
                            }
                          />
                        </BsReactNative.View>
                      </BsReactNative.View>
                      <BsReactNative.TouchableOpacity
                        style=styles##checkin onPress={_ => ()}>
                        <Text
                          message=messages##checkin
                          style=styles##checkInText
                        />
                      </BsReactNative.TouchableOpacity>
                    </BsReactNative.View>
                  </>
                )
              ->ReasonReact.array}
           </BsReactNative.ScrollView>;
         }}
      </BsReactNative.View>
    </BsReactNative.View>;
  },
};
