module Herochat = HerochatWidgetReactnative;

let messages =
  [@intl.messages]
  {
    "title": {
      "id": "chat.title",
      "defaultMessage": {j|Chat|j},
    },
    "placeholder": {
      "id": "chat.placeholder",
      "defaultMessage": {j|Napisz coś|j},
    },
    "error": {
      "id": "generic.error",
      "defaultMessage": {j|Coś poszło nie tak 😕|j},
    },
    "errorSubtitle": {
      "id": "generic.error.subtitle",
      "defaultMessage": {j|Pracujemy żeby to poprawić.|j},
    },
    "loadMoreTitle": {
      "id": "generic.error.loadMoreTitle",
      "defaultMessage": {j|Starsze wiadomości|j},
    },
  };

let styles =
  BsReactNative.(
    StyleSheet.create(
      Style.{
        "container":
          style([
            flex(1.0),
            backgroundColor(Theme.primary),
            width(Pct(100.0)),
          ]),
        "header":
          style([
            padding(Pt(16.0)),
            backgroundColor(Theme.primary),
            height(Pt(100.0)),
          ]),
        "headerText":
          style([
            marginTop(Pt(16.0)),
            fontWeight(`_600),
            color(Theme.light),
            fontSize(Float(15.0)),
          ]),
        "content":
          style([
            backgroundColor(Theme.light),
            flex(1.0),
            borderTopLeftRadius(20.0),
            borderTopRightRadius(20.0),
          ]),
      },
    )
  );

let avatar =
  `Required(BsReactNative.Packager.require("./assets/avatar.png"));

let sendIcon =
  `Required(BsReactNative.Packager.require("./assets/icon_send.png"));

let theme =
  Herochat.Theme.make(
    ~primary=Theme.primaryRaw,
    ~primaryAccent=Theme.primaryAccentRaw,
    ~sendIcon,
    (),
  );

let makeContext = (~clientId) => {
  Herochat.Config.make(
    ~instanceId="8db18c06eee947febfccb7e87aee9b70",
    ~conversationId=clientId,
    ~authorId=clientId,
    ~uri=Env.herochatUri,
    ~wsUri=Env.herochatWsUri,
    ~authType=Herochat.Auth.Raw,
    (),
  );
};

let logo = `Required(BsReactNative.Packager.require("./assets/lot.png"));
let plane = `Required(BsReactNative.Packager.require("./assets/plane.png"));

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~onUnauthorized, ~deviceToken, ~clientId, _) => {
  ...component,
  render: _ => {
    let%Epitath intl = children =>
      <ReactIntl.IntlInjector> ...children </ReactIntl.IntlInjector>;

    let config = makeContext(~clientId);

    <BsReactNative.View style=styles##container>
      <BsReactNative.View style=styles##header>
        <BsReactNative.Image source=logo />
        <Text message=messages##title style=styles##headerText />
      </BsReactNative.View>
      <BsReactNative.View style=styles##content>
        <Herochat.Widget
          attachmentFileName="Plik"
          onUnauthorized
          ?deviceToken
          theme
          config
          avatar
          bottomOffset=60
          loadMoreTitle={intl.formatMessage(messages##loadMoreTitle)}
          placeholder={intl.formatMessage(messages##placeholder)}
          unauthorizedErrorTitle={intl.formatMessage(messages##error)}
          unauthorizedErrorSubtitle={
            intl.formatMessage(messages##errorSubtitle)
          }
          queryErrorTitle={intl.formatMessage(messages##error)}
          queryErrorSubtitle={intl.formatMessage(messages##errorSubtitle)}
        />
      </BsReactNative.View>
    </BsReactNative.View>;
  },
};

let sendWelcomeReferral = (~clientId) => {
  let config = makeContext(~clientId);
  Herochat.Headless.sendReferral(~config, ~referralId="start", ());
};
