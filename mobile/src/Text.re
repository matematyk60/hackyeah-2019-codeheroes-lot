module Raw = {
  let styles =
    BsReactNative.(
      StyleSheet.create(
        Style.{"text": style([fontFamily("Montserrat-Regular")])},
      )
    );

  let component = ReasonReact.statelessComponent(__MODULE__);
  let make = (~content, ~style=?, _) => {
    ...component,
    render: _ =>
      <BsReactNative.Text style={Cn.make([styles##text, Cn.unpack(style)])}>
        {ReasonReact.string(content)}
      </BsReactNative.Text>,
  };
};

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~message, ~style=?, _) => {
  ...component,
  render: _ => {
    let%Epitath intl = children =>
      <ReactIntl.IntlInjector> ...children </ReactIntl.IntlInjector>;

    <Raw content={intl.formatMessage(message)} ?style />;
  },
};
