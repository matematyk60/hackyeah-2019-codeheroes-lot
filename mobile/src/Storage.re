open BsReactNative;

let createStorePersistance = (key, encode, decode) => {
  let persist = state => {
    let stringified = encode(state)->Json.stringify;
    AsyncStorage.setItem(key, stringified, ()) |> ignore;
  };

  let getState = () =>
    AsyncStorage.getItem(key, ())
    |> Repromise.Rejectable.fromJsPromise
    |> Repromise.Rejectable.map(result => {
         let decoded =
           result
           |> Option.map(_, Json.parseOrRaise)
           |> Option.flatMap(_, json =>
                switch (decode(json)) {
                | x => Some(x)
                | exception (Json.Decode.DecodeError(msg)) =>
                  Js.log("Error: " ++ msg);
                  None;
                }
              );

         Result.Ok(decoded);
       })
    |> Repromise.Rejectable.catch(error => {
         Js.log(error);
         Repromise.resolved(Result.Error(error));
       });

  (persist, getState);
};
