type fragmentTypes;
[@bs.module]
external introspectionQueryResultData: fragmentTypes = "../fragmentTypes.json";

type fetch;
[@bs.module "isomorphic-fetch"] external fetch: fetch = "fetch";

type t = ApolloClient.generatedApolloClient;

let logError: 'a => unit = [%bs.raw
  {|
      function(error) {
        if (error.graphQLErrors) {
          error.graphQLErrors.forEach(({ message, locations, path }) => {
            const locationStr = JSON.stringify(locations, null, 2);
            console.log(
             `[GraphQL error]: Message: ${message}, Location: ${locationStr}, Path: ${path}`
            );
          });
        }
        if (error.networkError) {
          console.log('[Network error]:', error.networkError);
        }
      }
    |}
];

let fragmentMatcher =
  ApolloInMemoryCache.createIntrospectionFragmentMatcher(
    ~data=introspectionQueryResultData,
  );

let authLink =
  ApolloLinks.apolloLinkAsyncSetContext(_ =>
    Js.Obj.empty()->Js.Promise.resolve
  );

let errorLink = ApolloLinks.createErrorLink(logError);

let inMemoryCache =
  ApolloInMemoryCache.createInMemoryCache(~fragmentMatcher, ());

let httpLink = uri => ApolloLinks.createHttpLink(~uri, ());

let client =
  ReasonApollo.createApolloClient(
    ~link=
      ApolloLinks.from([|authLink, errorLink, httpLink(Env.apiGraphql)|]),
    ~cache=inMemoryCache,
    (),
  );
