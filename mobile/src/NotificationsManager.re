type state = {previousToken: option(string)};

type action =
  | Initialize;

let component = ReasonReact.reducerComponent(__MODULE__);
let make = (~context: SessionContext.t, _) => {
  ...component,
  initialState: () => {previousToken: None},
  reducer: (action, _state: state) =>
    switch (action) {
    | Initialize =>
      ReasonReact.SideEffects(
        self =>
          Repromise.(
            Firebase.assurePermissions()
            |> Rejectable.andThen(Firebase.getToken)
            |> Rejectable.wait(deviceToken => {
                 context.setDeviceToken(deviceToken);

                 let tokenListener =
                   Firebase.getTokenListener(deviceToken =>
                     context.setDeviceToken(deviceToken)
                   );

                 self.onUnmount(() => tokenListener());

                 Firebase.getNotificationsListener();
               })
          ),
      )
    },
  didMount: self => {
    Firebase.setupAndroidChannel();
    self.send(Initialize);
  },
  render: _ => ReasonReact.null,
};
