type firebase;

type messaging;

type notifications;

type baseNotification;

type notificationAndroid;

type notification = {. "android": notificationAndroid};

type notificationOpened;

type channel;

type android;

[@bs.module] external firebase: firebase = "react-native-firebase";

[@bs.send] external messaging: firebase => messaging = "messaging";

[@bs.send] external notifications: firebase => notifications = "notifications";

[@bs.send]
external notificationsAndroid: firebase => {. "android": android} =
  "notifications";

[@bs.send]
external _hasPermission: messaging => Js.Promise.t(bool) = "hasPermission";

let hasPermission = messaging =>
  _hasPermission(messaging) |> Repromise.Rejectable.fromJsPromise;

[@bs.send]
external _requestPermission: messaging => Js.Promise.t(bool) =
  "requestPermission";

let requestPermission = messaging =>
  _requestPermission(messaging) |> Repromise.Rejectable.fromJsPromise;

[@bs.send] external _getToken: messaging => Js.Promise.t(string) = "getToken";

let getToken = () =>
  firebase->messaging->_getToken |> Repromise.Rejectable.fromJsPromise;

[@bs.send]
external _onTokenRefresh: (messaging, string => unit, unit) => unit =
  "onTokenRefresh";

[@bs.send]
external _onNotification: (notifications, 'a => unit) => unit =
  "onNotification";

[@bs.module "react-native-firebase"] [@bs.scope "notifications"] [@bs.new]
external _createNotification: unit => notification = "Notification";

[@bs.module "react-native-firebase"] [@bs.scope "notifications"] [@bs.new]
external _createNotificationAndroid: unit => {. "android": notification} =
  "Notification";

[@bs.send]
external _setChannelDescription: (channel, string) => channel =
  "setDescription";

[@bs.send]
external _setAndroidChannelId: (notificationAndroid, string) => notification =
  "setChannelId";

[@bs.send]
external _setSmallIcon: (notificationAndroid, string) => notification =
  "setSmallIcon";

[@bs.send]
external _setColor: (notificationAndroid, string) => notification = "setColor";

[@bs.val]
[@bs.module "react-native-firebase"]
[@bs.scope ("notifications", "Android", "Priority")]
external androidPriorityMax: int = "Max";

[@bs.send]
external _setPriority: (notificationAndroid, int) => notification =
  "setPriority";

[@bs.module "react-native-firebase"]
[@bs.scope ("notifications", "Android")]
[@bs.new]
external _createAndroidChannelInstance: (string, string, int) => channel =
  "Channel";

[@bs.module "react-native-firebase"] [@bs.scope "notifications"] [@bs.new]
external _createNotification: unit => notification = "Notification";

[@bs.val]
[@bs.module "react-native-firebase"]
[@bs.scope ("notifications", "Android", "Importance")]
external androidImportanceMax: int = "Max";

[@bs.send]
external _createAndroidChannel: (android, channel) => unit = "createChannel";

[@bs.send]
external _setNotificationId: (notification, string) => notification =
  "setNotificationId";

[@bs.send]
external _setTitle: (notification, string) => notification = "setTitle";

[@bs.send]
external _setBody: (notification, string) => notification = "setBody";

[@bs.send] external _setData: (notification, 'a) => notification = "setData";

[@bs.send]
external _setSound: (notification, string) => notification = "setSound";

[@bs.send]
external _displayNotification:
  (notifications, notification) => Js.Promise.t(unit) =
  "displayNotification";

[@bs.send]
external _onNotificationOpened: (notifications, 'a => unit) => unit =
  "onNotificationOpened";

[@bs.send]
external _getInitialNotification: notifications => Js.Promise.t('a) =
  "getInitialNotification";

let getTokenListener = (callback: string => unit) =>
  firebase->messaging->_onTokenRefresh(callback);

let getInitialNotification = () =>
  firebase
  ->notifications
  ->_getInitialNotification
  ->Repromise.Rejectable.fromJsPromise
  |> Repromise.Rejectable.andThen(notification =>
       notification->Js.Nullable.toOption->Repromise.Rejectable.resolved
     );

let assurePermissions = () =>
  Repromise.(
    firebase->messaging->hasPermission
    |> Rejectable.andThen(enabled =>
         enabled
           ? Rejectable.resolved()
           : firebase->messaging->requestPermission
             |> Rejectable.andThen(_ => Rejectable.resolved())
       )
  );

let androidChannel = "herochat-pko-channel";
let androidChannelDesciption = "LOT";

let setupAndroidChannel = () =>
  BsReactNative.(
    switch (Platform.os()) {
    | Platform.Android =>
      let channel =
        _createAndroidChannelInstance(
          androidChannel,
          androidChannelDesciption,
          androidImportanceMax,
        )
        ->_setChannelDescription(androidChannelDesciption);

      firebase->notificationsAndroid##android->_createAndroidChannel(channel);
    | _ => ()
    }
  );

let _buildNotification = notification => {
  open BsReactNative;

  let baseNotification =
    _createNotification()
    ->_setNotificationId(notification##notificationId)
    ->_setTitle(notification##title)
    ->_setBody(notification##body)
    ->_setData(notification##data)
    ->_setSound("default");

  switch (Platform.os()) {
  | Platform.Android =>
    baseNotification##android->_setAndroidChannelId(androidChannel)##android
    ->_setSmallIcon("ic_launcher")##android
    ->_setColor("#000000")##android
    ->_setPriority(androidPriorityMax)
  | Platform.IOS(_) => baseNotification
  };
};

let getNotificationsListener = () =>
  firebase
  ->notifications
  ->_onNotification(notification => {
      let localNotification = _buildNotification(notification);
      firebase
      ->notifications
      ->_displayNotification(localNotification)
      ->Repromise.Rejectable.fromJsPromise
      |> Repromise.Rejectable.catch(err => {
           Js.log(err);
           Repromise.resolved();
         })
      |> ignore;
    });

let getNotificationOpenedListener = callback =>
  firebase
  ->notifications
  ->_onNotificationOpened(notification =>
      notification->Js.Nullable.toOption->callback
    );
