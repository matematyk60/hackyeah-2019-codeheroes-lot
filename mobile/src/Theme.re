open BsReactNative.Style;

let padding1: pt_pct = Pt(16.0);
let padding2: pt_pct = Pt(32.0);

let light: string_interpolated = String("#fff");
let primaryRaw = "#242668";
let primary: string_interpolated = String(primaryRaw);
let primaryAccentRaw = "rgba(0, 53, 115, 0.15)";
let dark: string_interpolated = String("#4c4d4f");
let error: string_interpolated = String("#b70319");
let borderGreyRaw = "#dadce1";
let borderGrey: string_interpolated = String(borderGreyRaw);

let mint: string_interpolated = String("#53cab5");

let grey: string_interpolated = String("#7f7f7f");
