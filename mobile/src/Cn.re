let emptyStyle = BsReactNative.Style.style([]);

let make = cns => BsReactNative.StyleSheet.flatten(cns);

let ifTrue = (cn, x) => x ? cn : emptyStyle;

let ifSome = (cn, x) =>
  switch (x) {
  | Some(_) => cn
  | None => emptyStyle
  };

let mapSome = (x, fn) =>
  switch (x) {
  | Some(x) => x->fn
  | None => emptyStyle
  };

let unpack =
  fun
  | Some(x) => x
  | None => emptyStyle;
