type t =
  | Chat
  | Offer(string, string, string)
  | Bookings(string);
