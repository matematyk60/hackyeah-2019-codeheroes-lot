let styles =
  BsReactNative.(
    StyleSheet.create(
      Style.{
        "view":
          style([flex(1.0), justifyContent(Center), alignItems(Center)]),
      },
    )
  );

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~style=?, children) => {
  ...component,
  render: _ =>
    <BsReactNative.View style={Cn.make([styles##view, Cn.unpack(style)])}>
      ...children
    </BsReactNative.View>,
};
