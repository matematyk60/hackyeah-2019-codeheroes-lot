let styles = {
  open BsReactNative;
  open Style;
  let baseStyle =
    style([
      backgroundColor(Theme.error),
      borderRadius(21.5),
      padding(Pt(14.0)),
      alignItems(Center),
      justifyContent(Center),
    ]);

  StyleSheet.create({
    "primary": StyleSheet.flatten([baseStyle]),
    "inactive":
      StyleSheet.flatten([
        baseStyle,
        style([backgroundColor(Theme.borderGrey)]),
      ]),
    "text":
      style([
        fontSize(Float(15.0)),
        fontFamily("Montserrat-Bold"),
        color(Theme.light),
      ]),
    "sizeBig": style([width(Pct(100.0))]),
    "sizeSmall":
      style([padding(Pt(8.0)), paddingHorizontal(Theme.padding1)]),
    "sizeDefault": style([width(Pt(200.0))]),
    "sizeNone": style([]),
    "backButton":
      style([
        marginTop(Pt(16.0)),
        color(Theme.borderGrey),
        textAlign(Center),
        fontSize(Float(14.0)),
      ]),
  });
};

module Back = {
  let component = ReasonReact.statelessComponent(__MODULE__);
  let make = (~style=?, ~caption, ~onPress, _) => {
    ...component,
    render: _ => {
      let%Epitath intl = children =>
        <ReactIntl.IntlInjector> ...children </ReactIntl.IntlInjector>;

      <BsReactNative.TouchableOpacity onPress ?style>
        <BsReactNative.Text style=styles##backButton>
          {ReasonReact.string(intl.formatMessage(caption))}
        </BsReactNative.Text>
      </BsReactNative.TouchableOpacity>;
    },
  };
};

type type_ = [ | `primary];

type size = [ | `big | `default | `small | `none];

let getSizeStyles = size =>
  switch (size) {
  | `big => styles##sizeBig
  | `default => styles##sizeDefault
  | `small => styles##sizeNone
  | `none => styles##sizeNone
  };

let getWrapperSizeStyle = size =>
  switch (size) {
  | `big => styles##sizeNone
  | `default => styles##sizeNone
  | `small => styles##sizeSmall
  | `none => styles##sizeNone
  };

let component = ReasonReact.statelessComponent(__MODULE__);
let make =
    (
      ~caption: ReactIntl.message,
      ~onPress: unit => unit,
      ~size=`default,
      ~disabled=false,
      ~loading=false,
      ~style=?,
      _,
    ) => {
  ...component,
  render: _ => {
    let style = Cn.make([Cn.unpack(style), getSizeStyles(size)]);
    <BsReactNative.TouchableOpacity
      onPress={disabled ? _ => () : (_ => onPress())} style>
      <BsReactNative.View
        style={Cn.make([getWrapperSizeStyle(size), styles##primary])}>
        {loading
           ? <BsReactNative.ActivityIndicator />
           : <Text style=styles##text message=caption />}
      </BsReactNative.View>
    </BsReactNative.TouchableOpacity>;
  },
};
