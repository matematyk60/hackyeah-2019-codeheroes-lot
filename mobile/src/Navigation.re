module BottomNavigation = {
  let chatIcon =
    `Required(BsReactNative.Packager.require("./assets/chat.png"));

  let bookingIcon =
    `Required(BsReactNative.Packager.require("./assets/calendar.png"));

  let offersIcon =
    `Required(BsReactNative.Packager.require("./assets/ticket.png"));

  let moreIcon =
    `Required(BsReactNative.Packager.require("./assets/more.png"));

  let styles =
    BsReactNative.(
      StyleSheet.create(
        Style.{
          "container":
            style([
              justifyContent(SpaceBetween),
              alignItems(Center),
              height(Pt(60.0)),
              flexDirection(Row),
              borderWidth(1.0),
              borderColor(Theme.borderGrey),
              paddingLeft(Pt(16.0)),
              paddingRight(Pt(16.0)),
            ]),
          "icon": style([]),
          "iconWrapper":
            style([
              height(Pt(25.0)),
              justifyContent(Center),
              alignItems(Center),
            ]),
          "iconText": style([color(Theme.grey), fontSize(Float(8.0))]),
          "touchable": style([alignItems(Center), justifyContent(Center)]),
        },
      )
    );

  let messages =
    [@intl.messages]
    {
      "booking": {
        "id": "navigation.booking",
        "defaultMessage": {j|More rezerwacje|j},
      },
      "offers": {
        "id": "navigation.offers",
        "defaultMessage": {j|Kart pokłądowe|j},
      },
      "chat": {
        "id": "navigation.booking",
        "defaultMessage": {j|Chat|j},
      },
      "more": {
        "id": "navigation.booking",
        "defaultMessage": {j|Więcej|j},
      },
    };

  let component = ReasonReact.statelessComponent(__MODULE__);
  let make = (~navigate, _) => {
    ...component,
    render: _ =>
      <BsReactNative.View style=styles##container>
        <BsReactNative.TouchableOpacity
          style=styles##touchable onPress={_ => ()}>
          <BsReactNative.View style=styles##iconWrapper>
            <BsReactNative.Image style=styles##icon source=bookingIcon />
          </BsReactNative.View>
          <Text message=messages##booking style=styles##iconText />
        </BsReactNative.TouchableOpacity>
        <BsReactNative.TouchableOpacity
          style=styles##touchable onPress={_ => ()}>
          <BsReactNative.View style=styles##iconWrapper>
            <BsReactNative.Image style=styles##icon source=offersIcon />
          </BsReactNative.View>
          <Text message=messages##offers style=styles##iconText />
        </BsReactNative.TouchableOpacity>
        <BsReactNative.TouchableOpacity
          onPress={_ => navigate(Route.Chat)} style=styles##touchable>
          <BsReactNative.View style=styles##iconWrapper>
            <BsReactNative.Image style=styles##icon source=chatIcon />
          </BsReactNative.View>
          <Text message=messages##chat style=styles##iconText />
        </BsReactNative.TouchableOpacity>
        <BsReactNative.TouchableOpacity
          onPress={_ => ()} style=styles##touchable>
          <BsReactNative.View style=styles##iconWrapper>
            <BsReactNative.Image style=styles##icon source=moreIcon />
          </BsReactNative.View>
          <Text message=messages##more style=styles##iconText />
        </BsReactNative.TouchableOpacity>
      </BsReactNative.View>,
  };
};

let styles =
  BsReactNative.(StyleSheet.create(Style.{"content": style([flex(1.0)])}));

type state = {screen: Route.t};

type action =
  | Navigate(Route.t);

let component = ReasonReact.reducerComponent(__MODULE__);
let make = (~context: SessionContext.t, _) => {
  ...component,
  initialState: () => {
    screen: Bookings("1974500069317968"),
    // Offer(
    //   "facebook",
    //   "6a81e5f5-260e-4346-b2d2-1a1c49f0d350",
    //   "1974500069317968",
    //   "waw-sin-1",
    // ),
  },
  reducer: (action, _state) =>
    switch (action) {
    | Navigate(screen) => ReasonReact.Update({screen: screen})
    },
  didMount: self => {
    let prefix = "lot://";
    let handleUrl = url => {
      let url =
        url
        ->Js.String.replace(prefix, "", _)
        ->Js.String.split("/", _)
        ->List.fromArray;

      let route =
        Route.(
          switch (url) {
          | ["offers", _, chatbotId, _, offerId] =>
            let userId = Utils.uniqId();
            context.setId(userId);
            Herochat.sendWelcomeReferral(~clientId=userId)->ignore;
            Offer(chatbotId, userId, offerId);
          | _ => Chat
          }
        );

      self.send(Navigate(route));
    };

    BsReactNative.Linking.getInitialURL()
    |> Js.Promise.then_(url =>
         switch (url) {
         | None => Js.Promise.resolve()
         | Some(url) => handleUrl(url) |> Js.Promise.resolve
         }
       )
    |> ignore;

    BsReactNative.Linking.addEventListener("url", event =>
      handleUrl(event##url)
    );

    let handleOpenedNotification = payload => {
      switch (payload) {
      | Some(payload) =>
        let source =
          try (payload##notification##_data##source) {
          | _ => ""
          };

        switch (source) {
        | "herochat" => self.send(Navigate(Chat))
        | _ => ()
        };

      | None => ()
      };
    };

    Firebase.getNotificationOpenedListener(handleOpenedNotification);

    self.onUnmount(() =>
      BsReactNative.Linking.removeEventListener("url", event =>
        handleUrl(event##url)
      )
    );
  },
  render: self => {
    <BsReactNative.View style=styles##content>
      {switch (context.state.id) {
       | Id(clientId) =>
         switch (self.state.screen) {
         | Bookings(userId) => <Booking_View_List userId />
         | Offer(chatbotId, userId, offerId) =>
           <Offer_View
             chatbotId
             userId
             offerId
             navigate={route => self.send(Navigate(route))}
           />
         | Chat =>
           <Screen>
             <Herochat
               onUnauthorized={_ => ()}
               clientId
               deviceToken={context.state.deviceToken}
             />
           </Screen>
         }
       | _ => <Screen> <BsReactNative.ActivityIndicator /> </Screen>
       }}
      <BottomNavigation navigate={route => self.send(Navigate(route))} />
    </BsReactNative.View>;
  },
};
