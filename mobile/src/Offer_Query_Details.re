module Offer = [%graphql
  {|
  query($id: String!) {
    offer(id: $id) {
      id
      from
      outboundFlight {
        departureAirport
        arrivalAirport
        flightTime
        price
        departureTime
        arrivalTime
      }
      inboundFlight {
        departureAirport
        arrivalAirport
        flightTime
        price
        departureTime
        arrivalTime
      }
      totalPrice
      outboundDate
      returnDate
    }
  }
|}
];

module QueryComponent = ReasonApollo.CreateQuery(Offer);

let component = ReasonReact.statelessComponent(__MODULE__);
let make = (~offerId, children) => {
  ...component,
  render: _ => {
    let variables = Offer.make(~id=offerId, ())##variables;

    let%Epitath {result, _} =
      children =>
        <QueryComponent variables fetchPolicy="network-only">
          ...children
        </QueryComponent>;

    children(result);
  },
};
