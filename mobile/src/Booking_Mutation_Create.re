module Update = [%graphql
  {|
    mutation CreateBooking($chatbotId: String!, $userId: String!, $offerId: String!) {
      createBooking(platformId: "herochat", chatbotId: $chatbotId, userId: $userId, offerId: $offerId) @bsVariant {
        result {
          id
          offer {
            id
            from
            to
            outboundFlight {
              departureAirport
              arrivalAirport
              flightTime
              price
              departureTime
              arrivalTime
            }
            inboundFlight {
              departureAirport
              arrivalAirport
              flightTime
              price
              departureTime
              arrivalTime
            }
            totalPrice
            outboundDate
            returnDate
          }
        }
        errors
      }
    }
  |}
];

module MutationComponent = Mutation.Create(Update);

let component = ReasonReact.statelessComponent(__MODULE__);
let make = children => {
  ...component,
  render: _ => {
    <MutationComponent>
      ...{(mutation, _) =>
        children((~chatbotId, ~offerId, ~userId, ()) => {
          let variables =
            Update.make(~chatbotId, ~offerId, ~userId, ())##variables;

          Js.log2("userId", userId);

          mutation(~variables, ())
          |> MutationComponent.toDomain(~mapper=data => data##createBooking);
        })
      }
    </MutationComponent>;
  },
};
