open MomentRe;

let fromLong = Json.Decode.float;
let toLong = Json.Encode.float;

let toShortTime = timestamp => {
  let then_ = MomentRe.momentWithTimestampMS(timestamp);
  let now = momentNow();
  let daysAgo = diff(now, then_, `days);

  let format =
    switch (daysAgo) {
    | ago when ago < 1.0 => "HH:mm" // day
    | ago when ago < 7.0 => "ddd HH:mm" // week
    | ago when ago < 365.0 => "Do MMM" // year
    | _ => "Do MMM YYYY" // years
    };

  Moment.format(format, then_);
};

let toFullTime = timestamp =>
  timestamp
  ->MomentRe.momentWithTimestampMS
  ->Moment.format("dd DDD MMM, HH:mm", _);

let toMonthTime = timestamp =>
  timestamp->MomentRe.momentWithTimestampMS->Moment.format("MM/YY", _);

let toDailyTime = timestamp =>
  timestamp->MomentRe.momentWithTimestampMS->Moment.format("DD MMMM", _);

let toDetailedTime = timestamp =>
  timestamp
  ->MomentRe.momentWithTimestampMS
  ->Moment.format("HH:mm:ss DD/MM/YYYY", _);

let toHourTime = timestamp =>
  timestamp->MomentRe.momentWithTimestampMS->Moment.format("HH:mm", _);

let toFullHourTime = timestamp =>
  timestamp->MomentRe.momentWithTimestampMS->Moment.format("HH:mm:ss", _);

let toMoment = timestamp => MomentRe.momentWithTimestampMS(timestamp);

let initialOffset = () => {
  let initialOffsetFactor = 1000. *. 60. *. 5.; // Take 5 minutes from future in advance due to clock skews
  Js.Date.now() +. initialOffsetFactor;
};

let setHourAndMinutes = (~time, value) => {
  let hour = MomentRe.Moment.get(`hour, time);
  let minute = MomentRe.Moment.get(`minute, time);
  value |> MomentRe.Moment.setHour(hour) |> MomentRe.Moment.setMinute(minute);
};

let toMillis = MomentRe.Moment.valueOf;

let toJsonTimestamp = moment => moment->toMillis->toLong;

let add = (t, amount, unit) =>
  t->MomentRe.Moment.add(~duration=MomentRe.duration(amount, unit));

let subtract = (t, amount, unit) =>
  t->MomentRe.Moment.subtract(~duration=MomentRe.duration(amount, unit));

module Unit = {
  type t = [ | `days | `hours | `minutes | `seconds | `milliseconds];

  let toString =
    fun
    | `days => "d"
    | `hours => "h"
    | `minutes => "min"
    | `seconds => "s"
    | `milliseconds => "ms";

  let format = ((unit, value)) => {
    value->int_of_float->string_of_int ++ toString(unit);
  };
};

let toLargestUnit =
    (millis: float): ((Unit.t, float), option((Unit.t, float))) => {
  let duration = millis->int_of_float->durationMillis;
  let getValue = unit => {
    let fn =
      switch (unit) {
      | `days => Duration.asDays
      | `hours => Duration.asHours
      | `minutes => Duration.asMinutes
      | `seconds => Duration.asSeconds
      | `milliseconds => Duration.asMilliseconds
      };

    fn(duration);
  };

  let getModifier = unit =>
    switch (unit) {
    | `days => 24.0
    | `hours => 60.0
    | `minutes => 60.0
    | `seconds => 1000.0
    | `milliseconds => 1.0
    };

  [
    (`days, Some(`hours)),
    (`hours, Some(`minutes)),
    (`minutes, Some(`seconds)),
    (`seconds, Some(`milliseconds)),
    (`milliseconds, None),
  ]
  ->List.reduce(
      None,
      (acc, (upper, lower)) => {
        let upperValue = getValue(upper);
        switch (acc) {
        | None when upperValue > 1.0 =>
          let modifier = getModifier(upper);
          let upper = (upper, upperValue);

          let lower =
            lower->Option.map(unit => {
              let diff = Js.Math.floor_float(upperValue) *. modifier;
              (unit, getValue(unit) -. diff);
            });

          Some((upper, lower));
        | _ => acc
        };
      },
    )
  ->Option.getWithDefault(((`milliseconds, 0.0), None));
};

let toDuration = millis => {
  let (upper, lower) = toLargestUnit(millis);

  (
    Unit.format(upper)
    ++ " "
    ++ lower->Option.map(Unit.format)->Option.getWithDefault("")
  )
  ->Js.String.trim;
};
