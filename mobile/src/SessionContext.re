type id =
  | Id(string)
  | Loading
  | None;

type state = {
  deviceToken: option(string),
  id,
};

type action =
  | Id(string)
  | SetDeviceToken(string)
  | Error;

type t = {
  state,
  setDeviceToken: string => unit,
  setId: string => unit,
};

type persistedState = {id: option(string)};

let (persistState, getState) =
  Storage.createStorePersistance(
    "orlen_hackyeah_v1",
    (state: persistedState) =>
      Json.Encode.(
        [("id", state.id)]
        ->List.reduce([], (acc, item) =>
            switch (item) {
            | (key, Some(value)) => [(key, value->string), ...acc]
            | _ => acc
            }
          )
        ->object_
      ),
    (json): persistedState =>
      Json.Decode.{id: json |> optional(field("id", string))},
  );

let component = ReasonReact.reducerComponent(__MODULE__);
let make = children => {
  ...component,
  initialState: () => {deviceToken: None, id: Loading},
  reducer: (action, state) =>
    switch (action) {
    | SetDeviceToken(deviceToken) =>
      ReasonReact.Update({...state, deviceToken: Some(deviceToken)})

    | Id(id) =>
      ReasonReact.UpdateWithSideEffects(
        {...state, id: Id(id)},
        _ => persistState({id: Some(id)}),
      )

    | Error =>
      ReasonReact.UpdateWithSideEffects(
        {...state, id: None},
        _self => persistState({id: None}),
      )
    },
  didMount: self => {
    getState()
    |> Repromise.Rejectable.wait(result =>
         switch (result) {
         | Result.Ok(Some(({id: Some(id), _}: persistedState))) =>
           self.send(Id(id))
         | _ =>
           let clientId = Utils.uniqId();
           Herochat.sendWelcomeReferral(~clientId)->ignore;
           self.send(Id(clientId));
         }
       );
  },
  render: self => {
    children({
      setId: userId => self.send(Id(userId)),
      state: self.state,
      setDeviceToken: deviceToken => self.send(SetDeviceToken(deviceToken)),
    });
  },
};
