#!/bin/bash
sed -i "s/version := .*/version := \"${VERSION}\"/" build.sbt
sed -i "s/\"version\": [^}]*/\"version\": \"${VERSION}\"/" src/main/resources/logback.xml