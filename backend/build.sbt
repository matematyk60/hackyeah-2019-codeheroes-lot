version := "0.0"

enablePlugins(SbtNativePackager)
enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)

val baseSettings = Seq(
  organization := "pl.lot",
  name := "hack-lot-backend",
  scalaVersion := "2.12.10",
  resolvers ++= Dependencies.additionalResolvers,
  libraryDependencies ++= Dependencies.all,
  parallelExecution in Test := false,
  scalacOptions ++= CompilerOpts.scalacOptions
)

val dockerSettings = Seq(
  dockerBaseImage := "java:openjdk-8",
  daemonUser in Docker := "root",
  dockerRepository := Some("docker.codeheroes.io"),
  dockerExposedPorts := Seq(8080)
)

val protoSettings = Seq(
  PB.targets in Compile := Seq(
    scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value
  )
)

lazy val `nm-user-gateway` =
  project
    .in(file("."))
    .settings(baseSettings: _*)
    .settings(dockerSettings: _*)
    .settings(protoSettings: _*)

PB.protoSources in Compile := Seq(
  baseDirectory.value / "src" / "main" / "protobufs",
  baseDirectory.value / "chatplatform-protobufs" / "protocol",
  baseDirectory.value / "chatplatform-protobufs" / "plugins" / "common",
  baseDirectory.value / "chatplatform-protobufs" / "chatbot_service" / "command",
  baseDirectory.value / "chatplatform-protobufs" / "file_service"
)
