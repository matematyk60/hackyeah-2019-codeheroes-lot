package pl.lot.infrastructure.inmem.lot

import cats.effect.IO
import cats.effect.concurrent.Ref
import pl.lot.domain.lot.LotAccessToken

class LotAccessTokenHolder(ref: Ref[IO, LotAccessToken]) {

  def setToken(providerAccessToken: LotAccessToken): IO[Unit] = ref.set(providerAccessToken)

  def getToken: IO[LotAccessToken] = ref.get
}
