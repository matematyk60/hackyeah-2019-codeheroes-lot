package pl.lot.infrastructure.inmem.lot.offer

import java.time.{LocalDate, ZonedDateTime}

import pl.lot.domain.lot.{Flight, Offer}

import scala.collection.mutable

object LotOfferHolder {

  val offerSet: mutable.Set[Offer] = mutable.Set(
    Offer(
      id = "waw-sin-1",
      from = "Warsaw",
      to = "singapore",
      outboundDate = LocalDate.parse("2019-09-24"),
      returnDate = LocalDate.parse("2019-10-02"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "SIN",
        departureTime = ZonedDateTime.parse("2019-09-24T10:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-09-24T18:15:00+01:00[Europe/Paris]"),
        flightTime = 480,
        price = 1300L
      ),
      inboundFlight = Flight(
        departureAirport = "SIN",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-02T13:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-24T20:00:00+01:00[Europe/Paris]"),
        flightTime = 420,
        price = 1100L
      ),
      totalPrice = 2400L,
      imageUrl =
        "https://s.yimg.com/uu/api/res/1.2/JDrTIQfGQrq2B1vl7zqjMw--~B/aD0zOTA7dz02OTA7c209MTthcHBpZD15dGFjaHlvbg--/http://media.zenfs.com/en_SG/News/e27/Singapore_AI_tech.jpg"
    ),
    Offer(
      id = "waw-sin-2",
      from = "Warsaw",
      to = "singapore",
      outboundDate = LocalDate.parse("2019-10-15"),
      returnDate = LocalDate.parse("2019-10-21"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "SIN",
        departureTime = ZonedDateTime.parse("2019-10-15T09:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-15T17:30:00+01:00[Europe/Paris]"),
        flightTime = 496,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "SIN",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-21T15:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-21T22:30:00+01:00[Europe/Paris]"),
        flightTime = 450L,
        price = 900L
      ),
      totalPrice = 1900L,
      imageUrl = "https://img-s-msn-com.akamaized.net/tenant/amp/entityid/BBVQryD.img?h=417&w=624&m=6&q=60&o=f&l=f"
    ),
    Offer(
      id = "waw-sin-3",
      from = "Warsaw",
      to = "singapore",
      outboundDate = LocalDate.parse("2019-12-01"),
      returnDate = LocalDate.parse("2019-12-05"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "SIN",
        departureTime = ZonedDateTime.parse("2019-12-01T07:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-01T16:30:00+01:00[Europe/Paris]"),
        flightTime = 750L,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "SIN",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-12-05T14:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-05T21:00:00+01:00[Europe/Paris]"),
        flightTime = 420L,
        price = 1100L
      ),
      totalPrice = 2100L,
      imageUrl = "https://image.freepik.com/free-photo/singapore-city-landscape-marina-bay-view_28947-10.jpg"
    ),
    Offer(
      id = "waw-cdg-1",
      from = "Warsaw",
      to = "paris",
      outboundDate = LocalDate.parse("2019-09-24"),
      returnDate = LocalDate.parse("2019-10-02"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "CDG",
        departureTime = ZonedDateTime.parse("2019-09-24T10:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-09-24T12:00:00+01:00[Europe/Paris]"),
        flightTime = 480,
        price = 1300L
      ),
      inboundFlight = Flight(
        departureAirport = "CDG",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-02T13:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-24T14:30:00+01:00[Europe/Paris]"),
        flightTime = 420,
        price = 1100L
      ),
      totalPrice = 2400L,
      imageUrl = "https://wallpaperbro.com/img/387144.jpg"
    ),
    Offer(
      id = "waw-cdg-2",
      from = "Warsaw",
      to = "paris",
      outboundDate = LocalDate.parse("2019-10-15"),
      returnDate = LocalDate.parse("2019-10-21"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "CDG",
        departureTime = ZonedDateTime.parse("2019-10-15T09:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-15T11:00:00+01:00[Europe/Paris]"),
        flightTime = 496,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "CDG",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-21T15:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-21T16:30:00+01:00[Europe/Paris]"),
        flightTime = 450L,
        price = 900L
      ),
      totalPrice = 1900L,
      imageUrl = "https://static.puzzlefactory.pl/puzzle/195/277/original.jpg"
    ),
    Offer(
      id = "waw-cdg-3",
      from = "Warsaw",
      to = "paris",
      outboundDate = LocalDate.parse("2019-12-01"),
      returnDate = LocalDate.parse("2019-12-05"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "CDG",
        departureTime = ZonedDateTime.parse("2019-12-01T07:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-01T09:30:00+01:00[Europe/Paris]"),
        flightTime = 750L,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "CDG",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-12-05T14:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-05T15:30:00+01:00[Europe/Paris]"),
        flightTime = 420L,
        price = 1100L
      ),
      totalPrice = 2100L,
      "https://www.telegraph.co.uk/content/dam/Travel/hotels/europe/france/paris/paris-cityscape-overview-guide-xlarge.jpg"
    ),
    Offer(
      id = "waw-ord-1",
      from = "Warsaw",
      to = "chicago",
      outboundDate = LocalDate.parse("2019-09-24"),
      returnDate = LocalDate.parse("2019-10-02"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "ORD",
        departureTime = ZonedDateTime.parse("2019-09-24T10:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-09-24T18:15:00+01:00[Europe/Paris]"),
        flightTime = 480,
        price = 1300L
      ),
      inboundFlight = Flight(
        departureAirport = "ORD",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-02T13:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-24T20:00:00+01:00[Europe/Paris]"),
        flightTime = 420,
        price = 1100L
      ),
      totalPrice = 2400L,
      "http://hanaleikauaivacation.com/wp-content/uploads/parser/chicago-landscape-3.jpg"
    ),
    Offer(
      id = "waw-ord-2",
      from = "Warsaw",
      to = "chicago",
      outboundDate = LocalDate.parse("2019-10-15"),
      returnDate = LocalDate.parse("2019-10-21"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "ORD",
        departureTime = ZonedDateTime.parse("2019-10-15T09:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-15T17:30:00+01:00[Europe/Paris]"),
        flightTime = 496,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "ORD",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-21T15:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-21T22:30:00+01:00[Europe/Paris]"),
        flightTime = 450L,
        price = 900L
      ),
      totalPrice = 1900L,
      "https://s3.amazonaws.com/adweek-shorthand-editorial/chicagobrandstars18/assets/nycQE0UoG3/chicago-stars-skyline-2018-2560x1391.jpeg"
    ),
    Offer(
      id = "waw-ord-3",
      from = "Warsaw",
      to = "chicago",
      outboundDate = LocalDate.parse("2019-12-01"),
      returnDate = LocalDate.parse("2019-12-05"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "ORD",
        departureTime = ZonedDateTime.parse("2019-12-01T07:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-01T16:30:00+01:00[Europe/Paris]"),
        flightTime = 750L,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "ORD",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-12-05T14:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-05T21:00:00+01:00[Europe/Paris]"),
        flightTime = 420L,
        price = 1100L
      ),
      totalPrice = 2100L,
      "https://i0.wp.com/katanaphoto.com/wp-content/uploads/2013/02/DK1_1803.jpg?w=600"
    ),
    Offer(
      id = "waw-tlv-1",
      from = "Warsaw",
      to = "telaviv",
      outboundDate = LocalDate.parse("2019-09-24"),
      returnDate = LocalDate.parse("2019-10-02"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "TLV",
        departureTime = ZonedDateTime.parse("2019-09-24T10:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-09-24T18:15:00+01:00[Europe/Paris]"),
        flightTime = 480,
        price = 1300L
      ),
      inboundFlight = Flight(
        departureAirport = "TLV",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-02T13:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-24T20:00:00+01:00[Europe/Paris]"),
        flightTime = 420,
        price = 1100L
      ),
      totalPrice = 2400L,
      imageUrl = "https://www.touristisrael.com/wp-content/uploads/tel-aviv-1779649_960_720-001.jpg"
    ),
    Offer(
      id = "waw-tlv-2",
      from = "Warsaw",
      to = "telaviv",
      outboundDate = LocalDate.parse("2019-10-15"),
      returnDate = LocalDate.parse("2019-10-21"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "TLV",
        departureTime = ZonedDateTime.parse("2019-10-15T09:15:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-15T17:30:00+01:00[Europe/Paris]"),
        flightTime = 496,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "TLV",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-10-21T15:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-10-21T22:30:00+01:00[Europe/Paris]"),
        flightTime = 450L,
        price = 900L
      ),
      totalPrice = 1900L,
      imageUrl = "https://www.tel-aviv.gov.il/en/PublishingImages/ee51a30ab7e14697b64976402f9ce82c.jpg"
    ),
    Offer(
      id = "waw-tlv-3",
      from = "Warsaw",
      to = "telaviv",
      outboundDate = LocalDate.parse("2019-12-01"),
      returnDate = LocalDate.parse("2019-12-05"),
      outboundFlight = Flight(
        departureAirport = "WAW",
        arrivalAirport = "TLV",
        departureTime = ZonedDateTime.parse("2019-12-01T07:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-01T16:30:00+01:00[Europe/Paris]"),
        flightTime = 750L,
        price = 1000L
      ),
      inboundFlight = Flight(
        departureAirport = "TLV",
        arrivalAirport = "WAW",
        departureTime = ZonedDateTime.parse("2019-12-05T14:00:00+01:00[Europe/Paris]"),
        arrivalTime = ZonedDateTime.parse("2019-12-05T21:00:00+01:00[Europe/Paris]"),
        flightTime = 420L,
        price = 1100L
      ),
      totalPrice = 2100L,
      imageUrl = "https://m.lot.com/p/411g4dd4b041a1c01f5d23eggjie0e322hc/pl/pl/mobile-large.jpg"
    )
  )

}
