package pl.lot.infrastructure.inmem.lot.booking

import pl.lot.domain.lot.Booking
import pl.lot.domain.lot.Booking

import scala.collection.mutable

object BookingHolder {

  val bookingSet: mutable.Set[Booking] = mutable.Set()

}
