package pl.lot.infrastructure.inmem.actor

import akka.actor.typed.ActorSystem
import akka.util.Timeout
import com.chatbotize.plugin.request.Request
import pl.lot.infrastructure.inmem.actor.ActorRepository.{ChatCommand, ChatMessage, ScheduleNotifications}
import pl.lot.{InstanceId, UserId}
import hero.common.akka.typed.ProxyActor.ProxyCommand

class ActorRepository(
    implicit system: ActorSystem[ProxyCommand[UserId, ChatCommand]],
    timeout: Timeout
) {
  def scheduleNotifications(id: UserId, offerId: String) =
    system ! ProxyCommand(id, ScheduleNotifications(offerId))

  def send(request: Request): Unit =
    system ! ProxyCommand(
      UserId(request.platformId, request.chatbotId, request.userId),
      ChatMessage(InstanceId(request.instanceId), request.action)
    )
}

object ActorRepository {

  sealed trait ChatCommand

  case class ChatMessage(instanceId: InstanceId, action: Request.Action) extends ChatCommand
  case class ScheduleNotifications(offerId: String)                      extends ChatCommand
  case class SendWhatToTake(offerId: String)                             extends ChatCommand
  case class SendCheckIn(offerId: String)                                extends ChatCommand
  case class SendGate(offerId: String)                                   extends ChatCommand
}
