package pl.lot.infrastructure.http.lot

import java.time.{LocalDate, ZonedDateTime}

class HttpLotAvailabilityIntegration {

  def flights(): Unit = ???
}

object HttpLotAvailabilityIntegration {
  case class AvailabilityRequest(
      params: AvailabilityReqeustParams
  )

  case class AvailabilityReqeustParams(
      origin: List[String],
      destination: List[String],
      departureDate: List[LocalDate],
      returnDate: LocalDate,
      market: String = "PL",
      tripType: String = "R",
      adt: Int,
      c14: Int,
      chd: Int,
      inf: Int
  )

  case class AvailabilityResponse(
      data: List[List[Offer]]
  )

  case class Offer(
      totalPrice: TotalPrice,
      inbound: FlightBound,
      outbound: FlightBound,
      url: String
  )

  case class FlightBound(duration: Long, segments: List[FlightSegment], price: Double)

  case class FlightSegment(
      idInfoSegment: Int,
      departureAirport: String,
      arrivalAirport: String,
      departureDate: ZonedDateTime,
      arrivalDate: ZonedDateTime,
      flightNumber: Int
  )

  case class TotalPrice(
      price: Double,
      basePrice: Double,
      tax: Double,
      currency: String
  )
}
