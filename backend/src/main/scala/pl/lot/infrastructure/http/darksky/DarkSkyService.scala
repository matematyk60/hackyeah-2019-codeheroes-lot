package pl.lot.infrastructure.http.darksky

import cats.effect.IO
import pl.lot.infrastructure.http.darksky.DarkSkyService.WeatherResponse
import hero.common.logging.{Logger, LoggingSupport}
import io.circe.generic.auto._
import org.http4s.circe.jsonOf
import org.http4s.client._
import org.http4s.{EntityDecoder, Method, Request, Status, Uri}

import scala.util.Random

class DarkSkyService(client: Client[IO], apiKey: String)(implicit logger: Logger[IO]) extends LoggingSupport {

  def fetchWeather(latitude: String, longitude: String, date: Long) =
    client.fetch(
      Request[IO](
        uri = Uri
          .unsafeFromString(s"https://api.darksky.net/forecast/$apiKey/$latitude,$longitude,$date")
          .withQueryParam("units", "ca")
          .withQueryParam("exclude", "currently,minutely,hourly,alerts,flags"),
        method = Method.GET
      )
    ) { response =>
      response.status match {
        case Status.Ok =>
          response
            .attemptAs[WeatherResponse]
            .value
            .flatMap {
              case Left(failure) =>
                response
                  .as[String]
                  .flatMap(
                    body =>
                      logger
                        .warn(s"Failed to parse DarkSky response, body is: [$body]", failure.getCause())
                        .map(_ => None)
                  )
              case Right(weatherResponse) =>
                IO.pure(
                  if (weatherResponse.daily.data.isEmpty) None
                  else Some(weatherResponse.daily.data.head.toDomain)
                )
            }

        case other =>
          response
            .as[String]
            .flatMap(body => logger.warn(s"DarkSky API returned non 200 code [$other], body is: [$body]"))
            .map(_ => None)
      }
    }
}

object DarkSkyService {

  case class Weather(temp: Double, rainProbability: Int)

  case class WeatherResponse(daily: DailyResult)

  case class DailyResult(data: List[DailyData])

  case class DailyData(temperatureHigh: Double, precipProbability: Option[Double]) {
    def toDomain =
      Weather(
        temp = temperatureHigh,
        rainProbability = (precipProbability.getOrElse(List(0.0, 0.1, 0.23)(Random.nextInt(3))) * 100).toInt)
  }

  lazy implicit val GeocodingResponseEntityDecoder: EntityDecoder[IO, WeatherResponse] = jsonOf[IO, WeatherResponse]
}
