package pl.lot.infrastructure.http.google

import cats.effect.IO
import hero.common.logging.{Logger, LoggingSupport}
import io.circe.generic.auto._
import org.http4s.circe.jsonOf
import org.http4s.client._
import org.http4s.{EntityDecoder, Method, Request, Status, Uri}

class GeocodingCoordinatesService(client: Client[IO], googleApiKey: String)(implicit logger: Logger[IO])
    extends LoggingSupport {

  import GeocodingCoordinatesService._

  private val uriPath = Uri.uri("https://maps.googleapis.com/maps/api/geocode/json")

  def findCoordinatesForName(name: String): IO[Option[Coordinates]] =
    client.fetch(
      Request[IO](
        uri = uriPath.withQueryParam("key", googleApiKey).withQueryParam("address", name),
        method = Method.GET
      )
    ) { response =>
      response.status match {
        case Status.Ok =>
          response
            .attemptAs[GeocodingResponse]
            .value
            .flatMap {
              case Left(failure) =>
                response
                  .as[String]
                  .flatMap(
                    body =>
                      logger
                        .warn(s"Failed to parse google response, body is: [$body]", failure.getCause())
                        .map(_ => None)
                  )
              case Right(geocodingResponse) =>
                IO.pure(
                  if (geocodingResponse.results.isEmpty) None
                  else Some(geocodingResponse.results.head.geometry.location.toDomain)
                )
            }

        case other =>
          response
            .as[String]
            .flatMap(body => logger.warn(s"Google API returned non 200 code [$other], body is: [$body]"))
            .map(_ => None)
      }
    }
}

object GeocodingCoordinatesService {

  case class Coordinates(latitude: Double, longitude: Double)

  case class GeocodingResponse(results: List[GeocodingResult])

  case class GeocodingResult(geometry: GeocodingGeometry)

  case class GeocodingGeometry(location: GeocodingLocation)

  case class GeocodingLocation(lat: Double, lng: Double) {
    def toDomain: Coordinates = Coordinates(latitude = lat, longitude = lng)
  }

  lazy implicit val GeocodingResponseEntityDecoder: EntityDecoder[IO, GeocodingResponse] = jsonOf[IO, GeocodingResponse]
}
