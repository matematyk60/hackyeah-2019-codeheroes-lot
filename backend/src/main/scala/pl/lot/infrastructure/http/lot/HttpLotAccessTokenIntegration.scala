package pl.lot.infrastructure.http.lot

import cats.effect.IO
import cats.instances.string._
import cats.syntax.apply._
import pl.lot.ConfigValueProvider.LotApiConfig
import pl.lot.domain.lot.LotAccessToken
import pl.lot.infrastructure.http.lot.HttpLotAccessTokenIntegration.{AuthorizeRequest, AuthorizeResponse}
import hero.common.logging.{Logger, LoggingSupport}
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder}
import org.http4s.circe.CirceEntityDecoder._
import org.http4s.circe.CirceEntityEncoder._
import org.http4s.client.Client
import org.http4s.{EntityDecoder, EntityEncoder, Header, Headers, Method, Request, Status, Uri}
import pl.lot.ConfigValueProvider.LotApiConfig
class HttpLotAccessTokenIntegration(lotApiConfig: LotApiConfig, client: Client[IO]) extends LoggingSupport {

  private val uri = Uri.unsafeFromString("https://api.lot.com/flights-dev/v2/auth/token/get")

  private val authorizeRequest = Request[IO](
    uri = uri,
    method = Method.POST,
    headers = Headers(List(Header("X-Api-Key", lotApiConfig.apiKey)))
  ).withEntity(AuthorizeRequest(lotApiConfig.secretKey))

  def fetchToken(implicit logger: Logger[IO]): IO[LotAccessToken] = {
    client.fetch(authorizeRequest) {
      case response if response.status == Status.Ok =>
        response.attemptAs[AuthorizeResponse].value.flatMap {
          case Left(decodeFailure) =>
            response.bodyAsText.compile.foldMonoid
              .flatMap(
                body =>
                  logger.error(
                    s"Received status ok when fetching access token, but could not parse result.Decode failure: [$decodeFailure] Body is [$body]"
                  ) *> IO
                    .raiseError(new IllegalStateException(s"Could not fetch token"))
              )
          case Right(body) =>
            logger
              .info(s"Successfully fetched access token, expires in [${body.expires_in}] seconds")
              .map(_ => LotAccessToken(body.access_token))
        }

      case response =>
        response.bodyAsText.compile.foldMonoid.flatMap(
          body =>
            logger.error(
              s"Received non 200 status [${response.status}] when fetching access token, raising error.Response body is $body"
            ) *> IO.raiseError(new IllegalStateException(s"Could not fetch token"))
        )
    }

  }
}

object HttpLotAccessTokenIntegration {

  case class AuthorizeRequest(
      secret_key: String
  )

  case class AuthorizeResponse(
      access_token: String,
      expires_in: Int
  )

  implicit val authorizeRequestEncoder: Encoder[AuthorizeRequest] = deriveEncoder

  implicit val authorizeRequestEntityEncoder: EntityEncoder[IO, AuthorizeRequest] = circeEntityEncoder

  implicit val authorizeResponseDecoder: Decoder[AuthorizeResponse] = deriveDecoder

  implicit val authorizeResponseEntityDecoder: EntityDecoder[IO, AuthorizeResponse] = circeEntityDecoder

}
