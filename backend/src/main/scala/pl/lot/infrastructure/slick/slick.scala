package pl.lot.infrastructure

import hero.common.akka.journal.postgres.JournalTablesInitializer
import hero.common.jdbc.repository
import hero.common.postgres.PgCircePostgresProfile

package object slick {
  val repo                     = repository(PgCircePostgresProfile)
  val journalTablesInitializer = JournalTablesInitializer(PgCircePostgresProfile)
}
