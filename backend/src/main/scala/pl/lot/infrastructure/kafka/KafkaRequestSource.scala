package pl.lot.infrastructure.kafka

import cats.effect.{ContextShift, IO, Timer}
import com.chatbotize.plugin.request.Request
import com.typesafe.scalalogging.StrictLogging
import fs2.kafka.{consumerStream, AutoOffsetReset, ConsumerSettings}
import org.apache.kafka.common.serialization.{ByteArrayDeserializer, StringDeserializer}

import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.Try

class KafkaRequestSource(groupId: String, clientId: String, bootstrapServers: String, topic: String)(
    implicit cs: ContextShift[IO],
    timer: Timer[IO]
) extends StrictLogging {

  private val settings = ConsumerSettings(new StringDeserializer, new ByteArrayDeserializer)
    .withBootstrapServers(bootstrapServers)
    .withClientId(clientId)
    .withGroupId(groupId)
    .withEnableAutoCommit(true)
    .withAutoOffsetReset(AutoOffsetReset.Latest)
    .withAutoCommitInterval(10 seconds)

  def source: fs2.Stream[IO, Request] =
    consumerStream[IO]
      .using(settings)
      .evalTap(_.subscribeTo(topic))
      .flatMap(_.stream)
      .map(_.record)
      .map(record => {
        Try(Request.parseFrom(record.value())).fold(
          ex => {
            logger.error(s"Error while parsing value of record $record. Omitting...", ex)
            None
          },
          message => {
            logger.debug(s"Processing message: $message")
            Some(message)
          }
        )
      })
      .collect {
        case Some(message) => message
      }
}
