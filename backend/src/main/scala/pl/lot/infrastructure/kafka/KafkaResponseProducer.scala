package pl.lot.infrastructure.kafka

import cats.effect.{ContextShift, IO}
import cats.syntax.flatMap.toFlatMapOps
import com.chatbotize.plugin.response.Response
import pl.lot.UserId
import fs2.kafka.{Acks, KafkaProducer, ProducerSettings, _}
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

class KafkaResponseProducer(topic: String, producer: KafkaProducer[IO, String, Array[Byte]])(
    implicit cs: ContextShift[IO]) {

  private def getKey(response: Response): String =
    UserId(response.platformId, response.chatbotId, response.userId).value

  def send(response: Response): IO[ProducerResult[Id, String, Array[Byte], Unit]] = {
    val producerRecords = ProducerRecord(topic, getKey(response), response.toByteArray)
    val producerMessage = ProducerMessage.one(producerRecords)
    producer.produce(producerMessage).flatten
  }
}

object KafkaResponseProducer {

  def properties(bootstrapServers: String): ProducerSettings[String, Array[Byte]] =
    ProducerSettings[String, Array[Byte]](
      keySerializer = new StringSerializer,
      valueSerializer = new ByteArraySerializer
    ).withBootstrapServers(bootstrapServers).withAcks(Acks.All)
}
