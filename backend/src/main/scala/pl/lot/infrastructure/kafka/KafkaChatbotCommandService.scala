package pl.lot.infrastructure.kafka

import cats.effect.{ContextShift, IO}
import cats.syntax.flatMap.toFlatMapOps
import com.chatbotize.chatbot.metadata.command.Command
import fs2.kafka.{Acks, ProducerSettings, _}
import hero.common.logging.LoggingSupport
import org.apache.kafka.common.serialization.{ByteArraySerializer, StringSerializer}

class KafkaChatbotCommandService(kafkaTopic: String, bootstrapServers: String)(implicit cs: ContextShift[IO])
    extends LoggingSupport {

  private val properties = ProducerSettings[String, Array[Byte]](
    keySerializer = new StringSerializer,
    valueSerializer = new ByteArraySerializer
  ).withBootstrapServers(bootstrapServers).withAcks(Acks.All)

  private val messageProducerResource = producerResource[IO].using(properties)

  def send(command: Command) =
    messageProducerResource.use { producer =>
      val producerRecords = ProducerRecord(kafkaTopic, getKey(command), command.toByteArray)
      val producerMessage = ProducerMessage.one(producerRecords)
      producer.produce(producerMessage).flatten
    }

  private def getKey(command: Command): String = s"${command.chatbotId}"
}
