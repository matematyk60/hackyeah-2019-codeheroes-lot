package pl.lot

import com.typesafe.config.Config

object ConfigValueProvider {

  def provide(config: Config) = Configuration(
    ApplicationConfig(
      externalLogLevel = config.getString("log-level.external"),
      serviceLogLevel = config.getString("log-level.service")
    ),
    KafkaConfig(
      host = config.getString("kafka.host"),
      port = config.getInt("kafka.port")
    ),
    LotApiConfig(
      config.getString("lot.api-key"),
      config.getString("lot.secret-key")
    ),
    raw = config,
    config.getString("google.api-key"),
    config.getString("dark-sky.api-key"),
    config.getString("deeplink")
  )

  case class Configuration(
      app: ApplicationConfig,
      kafkaConfig: KafkaConfig,
      lotApiConfig: LotApiConfig,
      raw: Config,
      googleApiKey: String,
      darkSkyApiKey: String,
      deeplink: String
  )

  case class KafkaConfig(
      host: String,
      port: Int
  ) {
    def stringify: String = s"$host:$port"
  }

  case class ApplicationConfig(externalLogLevel: String, serviceLogLevel: String)

  case class LotApiConfig(apiKey: String, secretKey: String)
}
