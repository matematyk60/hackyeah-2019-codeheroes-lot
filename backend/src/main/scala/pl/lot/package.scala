package pl

package object lot {

  case class UserId(platformId: String, chatbotId: String, userId: String) {
    def value: String = s"$platformId-$chatbotId-$userId"
  }

  case class InstanceId(value: String) extends AnyVal

  object Chat {
    val PLUGIN_ID      = "dedicated-lot"
    val INSTANCE_BASIC = "basic"

    val instances = Map(
      INSTANCE_BASIC -> "Lot"
    )
  }
}
