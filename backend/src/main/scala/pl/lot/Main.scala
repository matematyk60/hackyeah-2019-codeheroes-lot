package pl.lot

import cats.effect.concurrent.Ref
import cats.effect.{ExitCode, IO, IOApp}
import pl.lot.domain.lot.LotAccessToken
import pl.lot.infrastructure.kafka.KafkaResponseProducer
import com.typesafe.config.ConfigFactory
import fs2.{kafka, Stream}
import hero.common.logging.Logger
import hero.common.logging.slf4j.ScalaLoggingLogger
import org.http4s.client.blaze.BlazeClientBuilder

import scala.concurrent.ExecutionContext.Implicits.global

object Main extends IOApp {

  private val config        = ConfigFactory.load("default.conf")
  private val configuration = ConfigValueProvider.provide(config)

  private implicit val logger: Logger[IO] = new ScalaLoggingLogger

  private val kafkaBootstrapServers = configuration.kafkaConfig.stringify

  override def run(args: List[String]): IO[ExitCode] = {
    for {
      client <- BlazeClientBuilder[IO](global).stream
      kafkaProducer <- kafka.producerStream[IO, String, Array[Byte]](
        KafkaResponseProducer.properties(kafkaBootstrapServers))
      accessTokenRef <- Stream.eval(Ref[IO].of(LotAccessToken("invalid")))
      app = new Application(configuration, client, accessTokenRef, kafkaProducer)
      _ <- Stream.eval(app.run())
    } yield ExitCode.Success

  }.compile.lastOrError
}
