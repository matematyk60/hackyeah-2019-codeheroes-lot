package pl.lot.api.graphql.schema.booking

import pl.lot.api.graphql.schema.offer.OfferTypes
import pl.lot.domain.lot.{Booking, BookingError}
import hero.common.util.time.TimeUtils.zonedDateTimeToMillis
import pl.lot.domain.lot.{Booking, BookingError}
import sangria.schema._
import sangria.macros.derive._

object BookingTypes {

  import pl.lot.api.graphql.schema.offer.OfferTypes._

  implicit val BookingType: ObjectType[Unit, Booking] = deriveObjectType(
    ReplaceField(
      "bookingTime",
      Field(
        "bookingTime",
        LongType,
        resolve = ctx => zonedDateTimeToMillis(ctx.value.bookingTime)
      )
    ),
    ReplaceField(
      "userId",
      Field(
        "userId",
        StringType,
        resolve = _.value.userId.value
      )
    )
  )

  implicit val BookingErrorType: EnumType[BookingError] = EnumType(
    "BookingError",
    values = List(EnumValue("NoSuchOffer", value = BookingError.NoSuchOffer))
  )

}
