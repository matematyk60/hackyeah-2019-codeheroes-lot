package pl.lot.api.graphql.schema

import pl.lot.api.graphql.service.GraphqlSecureContext
import sangria.schema._

object EmptyQuery {

  val empty: QueryHolder = new QueryHolder {
    override def queryFields(): List[Field[GraphqlSecureContext, Unit]] = fields[GraphqlSecureContext, Unit](
      Field(
        "nothing",
        StringType,
        arguments = Nil,
        resolve = _ => "elo420"
      )
    )
  }
}
