package pl.lot.api.graphql

import pl.lot.api.graphql.schema.EmptyQuery
import pl.lot.api.graphql.schema.booking.BookingQuery
import pl.lot.api.graphql.schema.offer.OfferQuery
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.api.graphql.schema.EmptyQuery
import pl.lot.api.graphql.schema.offer.OfferQuery
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import sangria.schema.ObjectType

import scala.concurrent.ExecutionContext

object Query {

  def query(geocodingCoordinatesService: GeocodingCoordinatesService, darkSkyService: DarkSkyService)(implicit ec: ExecutionContext): ObjectType[GraphqlSecureContext, Unit] = {
    val queryHolders = List(
      EmptyQuery.empty,
      OfferQuery.query(geocodingCoordinatesService, darkSkyService),
      BookingQuery.query()
    )

    ObjectType(
      name = "Query",
      fields = queryHolders.flatMap(_.queryFields())
    )
  }
}
