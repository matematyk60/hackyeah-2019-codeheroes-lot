package pl.lot.api.graphql.schema.offer

import java.time.ZoneOffset

import pl.lot.domain.lot.{Flight, Offer}
import hero.common.util.time.TimeUtils.zonedDateTimeToMillis
import sangria.macros.derive._
import sangria.schema._

object OfferTypes {

  implicit val FlightType: ObjectType[Unit, Flight] = deriveObjectType(
    ReplaceField(
      "departureTime",
      Field(
        "departureTime",
        LongType,
        resolve = ctx => zonedDateTimeToMillis(ctx.value.departureTime)
      )
    ),
    ReplaceField(
      "arrivalTime",
      Field(
        "arrivalTime",
        LongType,
        resolve = ctx => zonedDateTimeToMillis(ctx.value.arrivalTime)
      )
    )
  )

  implicit val OfferType: ObjectType[Unit, Offer] = deriveObjectType(
    ReplaceField(
      "outboundDate",
      Field(
        "outboundDate",
        LongType,
        resolve = ctx => zonedDateTimeToMillis(ctx.value.outboundDate.atStartOfDay(ZoneOffset.UTC))
      )
    ),
    ReplaceField(
      "returnDate",
      Field(
        "returnDate",
        LongType,
        resolve = ctx => zonedDateTimeToMillis(ctx.value.returnDate.atStartOfDay(ZoneOffset.UTC))
      )
    )
  )

}
