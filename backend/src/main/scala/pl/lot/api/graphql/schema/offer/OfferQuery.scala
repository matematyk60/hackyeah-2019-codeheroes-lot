package pl.lot.api.graphql.schema.offer

import cats.data.OptionT
import pl.lot.api.graphql.schema.QueryHolder
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder
import sangria.schema._

object OfferQuery {

  def query(geocodingCoordinatesService: GeocodingCoordinatesService, darkSkyService: DarkSkyService): QueryHolder =
    () =>
      fields[GraphqlSecureContext, Unit](
        {
          val IdArg = Argument("id", StringType)

          Field(
            "offer",
            OptionType(OfferTypes.OfferType),
            arguments = IdArg :: Nil,
            resolve = ctx => LotOfferHolder.offerSet.find(_.id == ctx.arg(IdArg))
          )
        }, {
          Field(
            "offers",
            ListType(OfferTypes.OfferType),
            arguments = Nil,
            resolve = _ => LotOfferHolder.offerSet.toList
          )
        }, {
          Field(
            "weather",
            StringType,
            resolve = _ => {
              val offer = LotOfferHolder.offerSet.head

              val response = for {
                coords <- OptionT(geocodingCoordinatesService.findCoordinatesForName("singapore"))
                weather <- OptionT(
                  darkSkyService.fetchWeather(
                    coords.latitude.toString,
                    coords.longitude.toString,
                    offer.outboundFlight.arrivalTime.toEpochSecond
                  ))
              } yield weather

              response
                .map(response => s"${response.rainProbability}    ${response.temp})")
                .value
                .map(_.getOrElse("unavailable"))
                .unsafeToFuture()
            }
          )
        }
    )

}
