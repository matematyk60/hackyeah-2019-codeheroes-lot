package pl.lot.api.graphql

import pl.lot.api.graphql.schema.MutationHolder
import pl.lot.api.graphql.schema.booking.BookingMutation
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.inmem.actor.ActorRepository
import pl.lot.api.graphql.schema.MutationHolder
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.inmem.actor.ActorRepository
import sangria.schema.ObjectType

import scala.concurrent.ExecutionContext

object Mutation {

  def mutation(actorRepository: ActorRepository)(implicit ec: ExecutionContext): ObjectType[GraphqlSecureContext, Unit] = {
    val mutationHolders: List[MutationHolder] = List(
      BookingMutation.mutation(actorRepository)
    )

    ObjectType(
      name = "Mutation",
      fields = mutationHolders.flatMap(_.mutationFields())
    )
  }
}
