package pl.lot.api.graphql.schema.booking

import java.time.{ZoneOffset, ZonedDateTime}

import pl.lot.UserId
import pl.lot.api.graphql.schema.MutationHolder
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.domain.lot.{Booking, BookingError}
import pl.lot.infrastructure.inmem.actor.ActorRepository
import pl.lot.infrastructure.inmem.lot.booking.BookingHolder
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder
import hero.common.sangria.mutation.MutationResultType
import hero.common.sangria.mutation.MutationResultType.MutationResult
import hero.common.util.IdProvider
import pl.lot.api.graphql.schema.MutationHolder
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.domain.lot.BookingError
import pl.lot.domain.lot.BookingError.NoSuchOffer
import pl.lot.infrastructure.inmem.actor.ActorRepository
import pl.lot.infrastructure.inmem.lot.booking.BookingHolder
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder
import sangria.schema._

import scala.util.Random

object BookingMutation {

  def mutation(actorRepository: ActorRepository): MutationHolder =
    () =>
      fields[GraphqlSecureContext, Unit]({
        val OfferIdArg    = Argument("offerId", StringType)
        val PlatformIdArg = Argument("platformId", StringType)
        val ChatbotIdArg  = Argument("chatbotId", StringType)
        val UserIdArg     = Argument("userId", StringType)

        Field(
          "createBooking",
          MutationResultType(BookingTypes.BookingType, BookingTypes.BookingErrorType, "CreateBookingMutation"),
          arguments = PlatformIdArg :: ChatbotIdArg :: UserIdArg :: OfferIdArg :: Nil,
          resolve = ctx => {
            val platformId = ctx.arg(PlatformIdArg)
            val chatbotId  = ctx.arg(ChatbotIdArg)
            val userId     = ctx.arg(UserIdArg)
            val offerId    = ctx.arg(OfferIdArg)

            val targetOffer = LotOfferHolder.offerSet.find(_.id == offerId)

            MutationResult.fromEitherOkOrError(targetOffer match {
              case Some(offer) =>
                val bookingToSave = Booking(
                  IdProvider.id.newId(),
                  UserId(platformId = platformId, chatbotId = chatbotId, userId = userId),
                  ZonedDateTime.now(ZoneOffset.UTC),
                  offer,
                  gateNumber = Random.nextInt(27) + 1
                )

                BookingHolder.bookingSet += bookingToSave
                actorRepository.scheduleNotifications(UserId(platformId, chatbotId, userId), offerId)
                Right(bookingToSave)
              case None =>
                Left(NoSuchOffer: BookingError)
            })
          }
        )
      })

}
