package pl.lot.api.graphql.core

import cats.effect.IO
import org.http4s.{Header, Headers, HttpRoutes, Response, Status}
import org.http4s.dsl.io._

object LotRedirectRoute {

  def route: HttpRoutes[IO] = HttpRoutes.of[IO] {
    case GET -> Root / platformId / chatbotId / userId / offerId =>
      IO(
        Response[IO](
          status = Status.MovedPermanently,
          headers = Headers(List(Header("Location", s"lot://offers/$platformId/$chatbotId/$userId/$offerId")))))
  }
}
