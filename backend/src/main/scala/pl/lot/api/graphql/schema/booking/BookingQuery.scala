package pl.lot.api.graphql.schema.booking

import pl.lot.api.graphql.schema.QueryHolder
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.inmem.lot.booking.BookingHolder
import pl.lot.api.graphql.schema.QueryHolder
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.inmem.lot.booking.BookingHolder
import sangria.schema.{Field, _}
object BookingQuery {

  def query(): QueryHolder = new QueryHolder {

    val UserIdArg = Argument("userId", StringType)
    override def queryFields(): List[Field[GraphqlSecureContext, Unit]] =
      fields[GraphqlSecureContext, Unit](
        Field(
          "myBookings",
          ListType(BookingTypes.BookingType),
          arguments = UserIdArg :: Nil,
          resolve = ctx => BookingHolder.bookingSet.find(_.userId.userId == ctx.arg(UserIdArg)).toList
        )
      )
  }
}
