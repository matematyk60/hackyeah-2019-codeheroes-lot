package pl.lot.api.graphql.core

import cats.effect.{ContextShift, IO}
import cats.syntax.applicativeError._
import pl.lot.api.graphql
import pl.lot.api.graphql.Mutation
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.infrastructure.inmem.actor.ActorRepository
import hero.common.logging.{Logger, LoggingSupport}
import io.circe.Json
import io.circe.optics.JsonPath._
import org.http4s.circe._
import org.http4s.dsl.io._
import org.http4s.headers.`WWW-Authenticate`
import org.http4s.{HttpRoutes, _}
import pl.lot.api
import pl.lot.api.graphql.Mutation
import pl.lot.api.graphql.service.GraphqlSecureContext
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.infrastructure.inmem.actor.ActorRepository
import sangria.execution._
import sangria.marshalling.circe._
import sangria.parser.QueryParser
import sangria.schema.Schema

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object GraphqlRoute extends LoggingSupport {
  private val authenticationErrorKey = "special-authentication-error-key"

  private val queryPath     = root.query.string
  private val operationPath = root.operationName.string
  private val variablesPath = root.variables.json

  private val errorsMessagePath = root.errors.each.message.string

  def route(geocodingCoordinatesService: GeocodingCoordinatesService, darkSkyService: DarkSkyService, actorRepository: ActorRepository)(implicit ec: ExecutionContext, cs: ContextShift[IO], logger: Logger[IO]): HttpRoutes[IO] = {

    val schema = Schema(api.graphql.Query.query(geocodingCoordinatesService, darkSkyService), mutation = Some(Mutation.mutation(actorRepository)))

    HttpRoutes.of[IO] {
      case request @ POST -> Root =>
        request.as[Json].flatMap { body =>
          val query     = queryPath.getOption(body)
          val operation = operationPath.getOption(body)

          val vars    = variablesPath.getOption(body) getOrElse Json.obj()
          val context = GraphqlSecureContext.instance

          val response = query.map(QueryParser.parse(_)) match {
            case Some(Success(value)) =>
              IO.fromFuture(
                  IO(
                    Executor
                      .execute(
                        schema,
                        value,
                        context,
                        variables = vars,
                        operationName = operation
                      )
                  )
                )
                .flatMap {
                  case value if errorsMessagePath.getAll(value).contains(authenticationErrorKey) =>
                    Unauthorized(`WWW-Authenticate`(Challenge(scheme = "Bearer", realm = "Token invalid")))
                  case value =>
                    Ok(value)
                }
                .onError {
                  case ex: Throwable => logger.error(s"Error while executing request [$request]", ex)
                }
                .recoverWith {
                  case error: QueryAnalysisError ⇒
                    BadRequest(error.resolveError)
                  case error: ErrorWithResolver ⇒
                    InternalServerError(error.resolveError)
                  case _ =>
                    InternalServerError()
                }
            case Some(Failure(error)) =>
              BadRequest(Json.obj("error" -> Json.fromString(error.getMessage)))
            case None =>
              BadRequest(Json.obj("error" -> Json.fromString("No query to execute")))
          }
          response
        }
    }
  }
}
