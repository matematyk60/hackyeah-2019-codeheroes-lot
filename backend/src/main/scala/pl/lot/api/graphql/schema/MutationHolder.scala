package pl.lot.api.graphql.schema

import pl.lot.api.graphql.service.GraphqlSecureContext
import sangria.schema.Field

trait MutationHolder {
  def mutationFields(): List[Field[GraphqlSecureContext, Unit]]
}
