package pl.lot.api.graphql.service

import scala.concurrent.ExecutionContext

trait GraphqlSecureContext {}

object GraphqlSecureContext {
  def instance(
      implicit executionContext: ExecutionContext
  ): GraphqlSecureContext =
    new GraphqlSecureContext {}
}
