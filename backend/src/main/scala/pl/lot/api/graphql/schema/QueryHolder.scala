package pl.lot.api.graphql.schema

import pl.lot.api.graphql.service.GraphqlSecureContext
import sangria.schema.Field

trait QueryHolder {
  def queryFields(): List[Field[GraphqlSecureContext, Unit]]
}
