package pl.lot.domain.lot

sealed trait BookingError

object BookingError {
  case object NoSuchOffer extends BookingError
}
