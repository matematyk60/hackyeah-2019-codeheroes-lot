package pl.lot.domain.lot

import java.time.{LocalDate, ZonedDateTime}

import pl.lot.UserId

case class LotAccessToken(value: String)

case class Booking(
    id: String,
    userId: UserId,
    bookingTime: ZonedDateTime,
    offer: Offer,
    gateNumber: Int
)

case class Offer(
    id: String,
    from: String,
    to: String,
    outboundDate: LocalDate,
    returnDate: LocalDate,
    outboundFlight: Flight,
    inboundFlight: Flight,
    totalPrice: Long,
    imageUrl: String
)

case class Flight(
    departureAirport: String,
    arrivalAirport: String,
    departureTime: ZonedDateTime,
    arrivalTime: ZonedDateTime,
    flightTime: Long,
    price: Long
)
