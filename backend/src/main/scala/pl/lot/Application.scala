package pl.lot

import java.time.Clock
import java.util.UUID

import akka.actor.ActorSystem
import akka.actor.typed.{ActorSystem => TypedActorSystem}
import akka.stream.ActorMaterializer
import akka.util.Timeout
import cats.effect.concurrent.Ref
import cats.effect.{ContextShift, IO, Timer}
import cats.syntax.parallel._
import pl.lot.ConfigValueProvider.Configuration
import pl.lot.api.graphql.core.{GraphqlRoute, LotRedirectRoute}
import pl.lot.app.lot.AccessTokenFetcherStream
import pl.lot.app.messaging.{ChatActor, MessageProcessor, ResponseService}
import pl.lot.domain.lot.LotAccessToken
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.infrastructure.http.lot.HttpLotAccessTokenIntegration
import pl.lot.infrastructure.inmem.actor.ActorRepository
import pl.lot.infrastructure.inmem.actor.ActorRepository.ChatCommand
import pl.lot.infrastructure.inmem.lot.LotAccessTokenHolder
import pl.lot.infrastructure.kafka.{KafkaChatbotCommandService, KafkaRequestSource, KafkaResponseProducer}
import fs2.kafka.KafkaProducer
import hero.common.akka.typed.ProxyActor.ProxyCommand
import hero.common.akka.typed.instances
import hero.common.logging.Logger
import hero.common.logging.slf4j.LoggingConfigurator
import org.http4s.HttpRoutes
import org.http4s.client.Client
import org.http4s.server.Router
import org.http4s.server.blaze.BlazeServerBuilder
import org.http4s.server.middleware.CORS
import org.http4s.syntax.kleisli.http4sKleisliResponseSyntax
import pl.lot.ConfigValueProvider.Configuration
import pl.lot.api.graphql.core.{GraphqlRoute, LotRedirectRoute}
import pl.lot.app.lot.AccessTokenFetcherStream
import pl.lot.app.messaging.{ChatActor, MessageProcessor, ResponseService}
import pl.lot.domain.lot.LotAccessToken
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.infrastructure.http.lot.HttpLotAccessTokenIntegration
import pl.lot.infrastructure.inmem.actor.ActorRepository
import pl.lot.infrastructure.inmem.actor.ActorRepository.ChatCommand
import pl.lot.infrastructure.inmem.lot.LotAccessTokenHolder
import pl.lot.infrastructure.kafka.{KafkaChatbotCommandService, KafkaRequestSource, KafkaResponseProducer}

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

class Application(
    config: Configuration,
    client: Client[IO],
    ref: Ref[IO, LotAccessToken],
    producer: KafkaProducer[IO, String, Array[Byte]])(
    implicit cs: ContextShift[IO],
    timer: Timer[IO],
    logger: Logger[IO],
    ec: ExecutionContext
) {

  LoggingConfigurator.setRootLogLevel(config.app.externalLogLevel)
  LoggingConfigurator.setLogLevel("com.nm", config.app.serviceLogLevel)

  private implicit val actorSystem  = ActorSystem()
  private implicit val materializer = ActorMaterializer()

  implicit val clock: Clock     = Clock.systemUTC()
  implicit val timeout: Timeout = Timeout(10 seconds)

  private implicit val accessTokenHolder: LotAccessTokenHolder = new LotAccessTokenHolder(ref)

  private implicit val accessTokenIntegration: HttpLotAccessTokenIntegration =
    new HttpLotAccessTokenIntegration(config.lotApiConfig, client)
  private implicit val accessTokenFetcherStream: fs2.Stream[IO, Unit] = AccessTokenFetcherStream.stream

  private val requestSource = new KafkaRequestSource(
    groupId = "plugin-dedicated-lot",
    clientId = UUID.randomUUID().toString,
    bootstrapServers = config.kafkaConfig.stringify,
    topic = "plugin-dedicated-lot-requests"
  )

  private val responseProducer = new KafkaResponseProducer(
    topic = "plugin-responses",
    producer
  )

  private val chatbotService = new KafkaChatbotCommandService(
    kafkaTopic = "chatbot-commands",
    bootstrapServers = config.kafkaConfig.stringify
  )

  private val coordinatesFinder: GeocodingCoordinatesService =
    new GeocodingCoordinatesService(client, config.googleApiKey)

  private val weatherService: DarkSkyService =
    new DarkSkyService(client, config.darkSkyApiKey)

  private val responseService = new ResponseService(responseProducer, config.deeplink)

  implicit val typedActorSystem: TypedActorSystem[ProxyCommand[UserId, ChatCommand]] =
    TypedActorSystem(
      instances
        .ioInstance[UserId, ChatCommand]
        .proxy(_.value, userId => ChatActor.behavior(userId, coordinatesFinder, weatherService, responseService)),
      "chat-proxy-actor"
    )

  private val actorRepository = new ActorRepository()

  private val chatMessageProcessor = new MessageProcessor(requestSource, actorRepository)

  private val graphqlRoute: HttpRoutes[IO] = GraphqlRoute.route(coordinatesFinder, weatherService, actorRepository)

  private val offersRoute: HttpRoutes[IO] = LotRedirectRoute.route

  private val routes = Router(
    "/graphql" -> graphqlRoute,
    "/offers"  -> offersRoute
  ).orNotFound

  val runServer: IO[Unit] =
    BlazeServerBuilder[IO]
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(CORS(routes))
      .serve
      .compile
      .drain

  def run(): IO[Unit] =
    (
      accessTokenFetcherStream.compile.drain,
      runServer,
      chatMessageProcessor.start().compile.drain
    ).parTupled.map(_ => ())
}
