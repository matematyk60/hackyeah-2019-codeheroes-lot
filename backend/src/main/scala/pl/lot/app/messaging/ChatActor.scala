package pl.lot.app.messaging

import akka.actor.typed.Behavior
import akka.actor.typed.scaladsl.Behaviors
import cats.data.OptionT
import cats.effect.{ContextShift, IO}
import com.chatbotize.plugin.request.Request.Action
import pl.lot.UserId
import pl.lot.infrastructure.http.darksky.DarkSkyService
import pl.lot.infrastructure.http.google.GeocodingCoordinatesService
import pl.lot.infrastructure.inmem.actor.ActorRepository._
import pl.lot.infrastructure.inmem.lot.booking.BookingHolder
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder
import com.typesafe.scalalogging.StrictLogging
import fs2.Stream
import pl.lot.infrastructure.inmem.actor.ActorRepository.ChatCommand
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder

import scala.concurrent.duration._
import scala.language.postfixOps

object ChatActor extends StrictLogging {

  def behavior(
      userId: UserId,
      geocodingCoordinatesService: GeocodingCoordinatesService,
      darkSkyService: DarkSkyService,
      responseService: ResponseService
  )(implicit cs: ContextShift[IO]): Behavior[ChatCommand] = Behaviors.receive[ChatCommand] {
    case (ctx, message) =>
      message match {
        case ChatMessage(instanceId, action) =>
          action match {
            case Action.Initialize(_) =>
              instanceId.value match {
                case "myreservations" =>
                  BookingHolder.bookingSet.filter(_.userId == userId).toList match {
                    case Nil =>
                      responseService.respondWithNoDataFound(userId, instanceId)
                      Behaviors.same
                    case bookings =>
                      responseService.respondWithYourBookings(userId, instanceId, bookings)
                      Behaviors.same
                  }

                case "whichgate" =>
                  val lastBooking = BookingHolder.bookingSet.filter(_.userId == userId).toList.headOption

                  lastBooking match {
                    case Some(booking) =>
                      responseService.respondWithGate(userId, instanceId, booking)
                      Behaviors.same
                    case None =>
                      responseService.respondWithNoDataFound(userId, instanceId)
                      Behaviors.same
                  }

                case "when" =>
                  val lastBooking = BookingHolder.bookingSet.filter(_.userId == userId).toList.headOption

                  lastBooking match {
                    case Some(booking) =>
                      responseService.respondWithDate(userId, instanceId, booking)
                      Behaviors.same
                    case None =>
                      responseService.respondWithNoDataFound(userId, instanceId)
                      Behaviors.same
                  }

                case _ =>
                  responseService.respondWithOffersWithWeather(
                    userId,
                    instanceId,
                    Stream
                      .emits(LotOfferHolder.offerSet.filter(_.to == instanceId.value).toSeq)
                      .covary[IO]
                      .map { offer =>
                        val weather: Option[DarkSkyService.Weather] = (for {
                          coordinates <- OptionT(geocodingCoordinatesService.findCoordinatesForName(instanceId.value))
                          weather <- OptionT(
                            darkSkyService.fetchWeather(
                              coordinates.latitude.toString,
                              coordinates.longitude.toString,
                              offer.outboundFlight.arrivalTime.toEpochSecond
                            )
                          )
                        } yield weather).value.unsafeRunSync()

                        (offer, weather)
                      }
                      .compile
                      .toList
                      .unsafeRunSync()
                  )

                  Behaviors.same
              }

            case Action.Message(value) =>
              responseService.respondWithUnexpected(userId, instanceId, value.message)
              Behaviors.same

            case Action.Abort(_) =>
              responseService.respondWithLoudFinish(userId, instanceId)
              Behaviors.same

            case Action.Empty =>
              responseService.respondWithLoudFinish(userId, instanceId)
              Behaviors.same
          }

        case ScheduleNotifications(offerId) =>
          ctx.scheduleOnce(1 minute, ctx.self, SendWhatToTake(offerId))
          ctx.scheduleOnce(100 minute, ctx.self, SendCheckIn(offerId))
          ctx.scheduleOnce(200 minute, ctx.self, SendGate(offerId))
          Behaviors.same

        case SendGate(offerId) =>
          responseService.sendGate(userId, offerId)
          Behaviors.same

        case SendCheckIn(offerId) =>
          responseService.sendCheckIn(userId, offerId)
          Behaviors.same

        case SendWhatToTake(offerId) =>
          responseService.sendWhatToTake(userId, offerId)
          Behaviors.same
      }
  }
}
