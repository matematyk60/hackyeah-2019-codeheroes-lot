package pl.lot.app.messaging

import java.time.format.DateTimeFormatter

import akka.stream.ActorMaterializer
import cats.effect.{ContextShift, IO}
import com.chatbotize.plugin.response.{ActionLoudFinish, ActionResponse, ActionUnexpected, Response}
import com.chatbotize.protocol.request.{Message => RequestMessage}
import com.chatbotize.protocol.response.Message.Payload
import com.chatbotize.protocol.response.{ActionButton, GenericTemplate, MessageTag, Text, Message => ResponseMessage}
import pl.lot.Chat._
import pl.lot.domain.lot.{Booking, Offer}
import pl.lot.infrastructure.http.darksky.DarkSkyService.Weather
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder
import pl.lot.infrastructure.kafka.KafkaResponseProducer
import pl.lot.{InstanceId, UserId}
import com.typesafe.scalalogging.StrictLogging
import pl.lot.domain.lot.{Booking, Offer}
import pl.lot.infrastructure.inmem.lot.offer.LotOfferHolder

import scala.concurrent.ExecutionContext

class ResponseService(responseProducer: KafkaResponseProducer, apiUrl: String)(
    implicit ec: ExecutionContext,
    mat: ActorMaterializer,
    cs: ContextShift[IO]
) extends StrictLogging {

  def respondWithLoudFinish(userId: UserId, instanceId: InstanceId): Unit =
    respond(userId, instanceId, Response.Action.LoudFinish(ActionLoudFinish()))

  def respondWithUnexpected(userId: UserId, instanceId: InstanceId, message: RequestMessage): Unit =
    respond(
      userId,
      instanceId,
      Response.Action.Unexpected(ActionUnexpected(message))
    )

  def respondWithYourBookings(userId: UserId, instanceId: InstanceId, bookings: List[Booking]) =
    respond(
      userId,
      instanceId,
      toResponse(Payload.Generic(GenericTemplate(bookings.map { booking =>
        GenericTemplate.Element(
          title = s"Z Warszawy do ${booking.offer.to match {
            case "telaviv"   => "Tel Avivu"
            case "singapore" => "Singapuru"
            case "paris"     => "Paryża"
            case "chicago"   => "Chicago"
          }}",
          image = Some(GenericTemplate.Image(is = GenericTemplate.Image.Is.Url(booking.offer.imageUrl))),
          subtitle = Some(
            s"${booking.offer.outboundDate.format(formatter)} - ${booking.offer.returnDate
              .format(formatter)}\nCena: ${booking.offer.totalPrice} zł"
          )
        )
      })))
    )

  def respondWithNoDataFound(userId: UserId, instanceId: InstanceId) =
    respond(
      userId,
      instanceId,
      toResponse(Payload.Text(Text(s"Nie posiadasz jeszcze żadnych rezerwacji.")))
    )

  def respondWithDate(userId: UserId, instanceId: InstanceId, booking: Booking) =
    respond(
      userId,
      instanceId,
      toResponse(Payload.Text(Text(s"Data Twojego wylotu do ${booking.offer.to match {
        case "telaviv"   => "Tel Avivu"
        case "singapore" => "Singapuru"
        case "paris"     => "Paryża"
        case "chicago"   => "Chicago"
      }} - ${booking.offer.outboundDate.format(formatter)}.")))
    )

  def respondWithGate(userId: UserId, instanceId: InstanceId, booking: Booking) =
    respond(
      userId,
      instanceId,
      toResponse(Payload.Text(Text(s"Numer bramki dla Twojego lotu do ${booking.offer.to match {
        case "telaviv"   => "Tel Avivu"
        case "singapore" => "Singapuru"
        case "paris"     => "Paryża"
        case "chicago"   => "Chicago"
      }} - ${booking.gateNumber}")))
    )

  def sendWhatToTake(userId: UserId, offerId: String) = {
    val offer = LotOfferHolder.offerSet.filter(_.id == offerId).head

    respond(
      userId,
      InstanceId(INSTANCE_BASIC),
      toResponse(Payload.Text(Text(s"Niedługo lecisz z ${offer.from match {
        case "Warsaw" => "Warszawy"
      }} do ${offer.to match {
        case "telaviv"   => "Tel Avivu"
        case "singapore" => "Singapuru"
        case "paris"     => "Paryża"
        case "chicago"   => "Chicago"
      }}. Przygotowując sie do podróży nie zapomnij zastosować się do obowiązujących reguł.")))
    )
  }

  def sendCheckIn(userId: UserId, offerId: String) = {
    val offer = LotOfferHolder.offerSet.filter(_.id == offerId).head

    respond(
      userId,
      InstanceId(INSTANCE_BASIC),
      toResponse(Payload.Text(Text(s"Zbliża się czas odprawy Twojego lotu z ${offer.from match {
        case "Warsaw" => "Warszawy"
      }} do ${offer.to match {
        case "telaviv"   => "Tel Avivu"
        case "singapore" => "Singapuru"
        case "paris"     => "Paryża"
        case "chicago"   => "Chicago"
      }}. Zaoszczędź czas korzystając z odprawy online.")))
    )
  }

  def sendGate(userId: UserId, offerId: String) =
    respond(
      userId,
      InstanceId(INSTANCE_BASIC),
      toResponse(
        Payload.Text(
          Text(s"Twój samolot do ${LotOfferHolder.offerSet.filter(_.id == offerId).head.to match {
            case "telaviv"   => "Tel Avivu"
            case "singapore" => "Singapuru"
            case "paris"     => "Paryża"
            case "chicago"   => "Chicago"
          }} niedługo startuje. Kieruj się do gate'u.")
        )
      )
    )

  def toResponse(payload: Payload): Response.Action = Response.Action.Response(
    ActionResponse(ResponseMessage(MessageTag.DEFAULT, payload) :: Nil)
  )

  private val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

  def respondWithOffersWithWeather(
      id: UserId,
      instanceId: InstanceId,
      offersWithWeather: List[(Offer, Option[Weather])]
  ): Unit =
    respond(
      id,
      instanceId,
      toResponse(Payload.Generic(GenericTemplate(offersWithWeather.map {
        case (offer, weather) =>
          GenericTemplate.Element(
            title = s"${offer.outboundDate.format(formatter)} - ${offer.returnDate.format(formatter)}",
            image = Some(GenericTemplate.Image(is = GenericTemplate.Image.Is.Url(offer.imageUrl))),
            subtitle = Some(
              s"Cena: ${offer.totalPrice} zł" + weather
                .map(weather => s"\n${weather.temp}°C\nSzansa opadów: ${weather.rainProbability}%")
                .getOrElse("")
            ),
            defaultAction = None,
            buttons = Seq(
              ActionButton(
                ActionButton.Type.Url(
                  ActionButton
                    .Url(
                      "Wybierz",
                      s"$apiUrl/${id.platformId}/${id.chatbotId}/${id.userId}/${offer.id}",
                      passContext = false
                    )
                )
              )
            )
          )
      })))
    )

  private def respond(userId: UserId, instanceId: InstanceId, actions: Response.Action*): Unit =
    fs2.Stream
      .emits(actions.toList)
      .covary[IO]
      .mapAsync(1)(
        action =>
          responseProducer
            .send(
              Response(
                pluginId = PLUGIN_ID,
                instanceId = instanceId.value,
                platformId = userId.platformId,
                chatbotId = userId.chatbotId,
                userId = userId.userId,
                action = action
              )
            )
      )
      .compile
      .drain
      .unsafeRunSync()
}
