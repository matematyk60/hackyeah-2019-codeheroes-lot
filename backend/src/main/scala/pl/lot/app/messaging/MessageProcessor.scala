package pl.lot.app.messaging

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import cats.effect.{IO, Timer}
import pl.lot.infrastructure.inmem.actor.ActorRepository
import pl.lot.infrastructure.kafka.KafkaRequestSource
import com.typesafe.scalalogging.StrictLogging
import fs2.Stream
import pl.lot.infrastructure.inmem.actor.ActorRepository

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

class MessageProcessor(requestSource: KafkaRequestSource, actorRepository: ActorRepository)(
    implicit system: ActorSystem,
    mat: ActorMaterializer,
    ec: ExecutionContext,
    timer: Timer[IO]
) extends StrictLogging {

  def start(): Stream[IO, Unit] = {
    requestSource.source
      .map(request => {
        actorRepository
          .send(request)
      })
      .handleErrorWith { error =>
        logger.error("Error while processing chatbot requests", error)
        start().delayBy(5 seconds)
      }
  }

  def initialize(): Unit = start()
}
