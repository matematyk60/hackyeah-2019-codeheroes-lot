package pl.lot.app.lot

import cats.effect.{IO, Timer}
import pl.lot.infrastructure.http.lot.HttpLotAccessTokenIntegration
import pl.lot.infrastructure.inmem.lot.LotAccessTokenHolder
import fs2.Stream
import hero.common.logging.{Logger, LoggingSupport}
import pl.lot.infrastructure.http.lot.HttpLotAccessTokenIntegration
import pl.lot.infrastructure.inmem.lot.LotAccessTokenHolder

import scala.concurrent.duration._
import scala.language.postfixOps

object AccessTokenFetcherStream extends LoggingSupport {

  def stream(
      implicit accessTokenHolder: LotAccessTokenHolder,
      accessTokenIntegration: HttpLotAccessTokenIntegration,
      logger: Logger[IO],
      timer: Timer[IO]): Stream[IO, Unit] = {
    def fetchTokenAndSave =
      for {
        token <- accessTokenIntegration.fetchToken
        _     <- accessTokenHolder.setToken(token)
        _     <- Logger[IO].info("Successfully saved new provider access token")
      } yield ()

    Stream
      .eval(fetchTokenAndSave)
      .handleErrorWith(
        ex =>
          Stream
            .eval(Logger[IO].error("Error encountered in access token fetcher stream, restarting", ex))
            .append(stream.delayBy(1 minute))
      )
      .onComplete(stream.delayBy(1 minute))

  }

}
