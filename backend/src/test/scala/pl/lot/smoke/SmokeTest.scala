package pl.lot.smoke

import cats.effect.{ContextShift, IO, Timer}
import pl.lot.{Application, ConfigValueProvider}
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import hero.common.logging.Logger
import hero.common.logging.slf4j.ScalaLoggingLogger
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpec}

import scala.concurrent.ExecutionContext.global

class SmokeTest extends WordSpec with BeforeAndAfterAll with StrictLogging with Matchers {

  implicit val cs: ContextShift[IO] = IO.contextShift(global)
  implicit val timer: Timer[IO]     = IO.timer(global)
  implicit val logger2: Logger[IO]  = new ScalaLoggingLogger

  private val config =
    ConfigFactory
      .load("test.conf")

  private val configuration = ConfigValueProvider.provide(config)

  "The gateway" should {
    "should be able to create application" in {
//      new Application(configuration)
    }
  }

}
