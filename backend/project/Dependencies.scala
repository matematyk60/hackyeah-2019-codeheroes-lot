import sbt._
import scalapb.compiler.Version

object Dependencies {

  val http4sVersion            = "0.20.3"
  val circeVersion             = "0.11.1"
  val circeOpticsVersion       = "0.11.0"
  val enumeratumVersion        = "1.5.13"
  val logbackVersion           = "1.2.3"
  val scalaLoggingVersion      = "3.9.2"
  val codeheroesCommonsVersion = "0.99"
  val jwtVersion               = "3.0.1"
  val catsVersion              = "1.6.1"
  val akkaVersion              = "2.5.8"
  val akkaActorVersion         = "2.5.25"
  val scalaTestVersion         = "3.0.8"
  val logbackLogstashVersion   = "6.1"

  val fs2Version         = "1.0.4"
  val fs2KafkaVersion    = "0.19.9"
  val kafkaClientVersion = "2.2.1"

  val grpcNettyVersion: String   = Version.grpcJavaVersion
  val grpcRuntimeVersion: String = Version.scalapbVersion

  private val fs2Dependencies = Seq(
    "co.fs2"           %% "fs2-core"     % fs2Version,
    "co.fs2"           %% "fs2-io"       % fs2Version,
    "com.ovoenergy"    %% "fs2-kafka"    % fs2KafkaVersion,
    "org.apache.kafka" % "kafka-clients" % kafkaClientVersion
  )

  private val http4sDependencies = Seq(
    "org.http4s"   %% "http4s-blaze-server" % http4sVersion,
    "org.http4s"   %% "http4s-blaze-client" % http4sVersion,
    "org.http4s"   %% "http4s-circe"        % http4sVersion,
    "org.http4s"   %% "http4s-dsl"          % http4sVersion,
    "io.circe"     %% "circe-generic"       % circeVersion,
    "io.circe"     %% "circe-optics"        % circeOpticsVersion,
    "com.beachape" %% "enumeratum"          % enumeratumVersion
  )

  private val loggingDependencies = Seq(
    "ch.qos.logback"             % "logback-classic" % logbackVersion,
    "ch.qos.logback"             % "logback-core"    % logbackVersion,
    "com.typesafe.scala-logging" %% "scala-logging"  % scalaLoggingVersion
  )

  private val catsDependencies = Seq(
    "org.typelevel" %% "cats-core" % catsVersion
  )

  private val commonDependencies = Seq(
    "io.codeheroes" %% "commons-sangria"           % codeheroesCommonsVersion,
    "io.codeheroes" %% "commons-test"              % codeheroesCommonsVersion,
    "io.codeheroes" %% "commons-akka-typed"        % codeheroesCommonsVersion,
    "io.codeheroes" %% "commons-postgres"          % codeheroesCommonsVersion
  )

  private val akkaDependencies = Seq(
    "com.typesafe.akka" %% "akka-typed"        % akkaVersion,
    "com.typesafe.akka" %% "akka-actor-typed"  % akkaActorVersion,
    "com.typesafe.akka" %% "akka-stream-typed" % akkaActorVersion,
    "com.typesafe.akka" %% "akka-slf4j"        % akkaVersion,
    "com.typesafe.akka" %% "akka-remote"       % akkaVersion
  )

  private val testDependencies = Seq(
    "org.scalatest" %% "scalatest" % scalaTestVersion % Test
  )

  val all: Seq[ModuleID] =
    http4sDependencies ++
      loggingDependencies ++
      commonDependencies ++
      catsDependencies ++
      testDependencies ++
      akkaDependencies ++
      fs2Dependencies

  val additionalResolvers: Seq[Resolver] = Seq(
    Resolver.jcenterRepo,
    Resolver.mavenCentral,
    Resolver.bintrayRepo("codeheroes", "maven"),
    "Typesafe Repo" at "http://repo.typesafe.com/typesafe/releases/"
  )
}
