addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.3.2")
addSbtPlugin("io.get-coursier"  % "sbt-coursier"        % "1.0.3")

addSbtPlugin("ch.epfl.scala"    % "sbt-bloop"    % "1.2.5")
addSbtPlugin("com.thesamet"     % "sbt-protoc"   % "0.99.20")
addSbtPlugin("org.scalameta"    % "sbt-scalafmt" % "2.0.0")
addSbtPlugin("com.timushev.sbt" % "sbt-updates"  % "0.4.1")

libraryDependencies += "com.thesamet.scalapb" %% "compilerplugin" % "0.9.0"
